<?php
return [
    'timeZone' => 'Asia/Baku',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                // ...
                # global rules begin
                [
                    'pattern'   => 'export-<id:\d+>',
                    'route'     => 'tour/export',
                    'suffix'    => '.pdf',
                ],
                # global rules end

                # frontend rules begin
                '/'             => 'site/index',
                '/login'        => 'site/login',
                '/export-all'   => 'tour/export',
                '/tours'        => 'tour/index',
                '/add'          => 'orders/createajax',
                # frontend rules end


                # backend rules begin

                # backend rules end

                #'<controller:\w+>/<id:\d+>'                 => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'    => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'             => '<controller>/<action>',

            ],
        ],
    ],
];
