<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $'partner_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Partners $partners
 */
class Users extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE   = 'create';
    #const SCENARIO_UPDATE   = 'update';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username',  'email','status'], 'required'],//'auth_key',, 'created_at', 'updated_at'
            ['username', 'match', 'pattern' => '/^[a-z]\w*$/i'],
            ['password_hash', 'required', 'on' => self::SCENARIO_CREATE],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            ['password_hash', 'string', 'min' => 6],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['password_reset_token'], 'unique'],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'Partnyor',
            'username' => 'İstifadəçi adı',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Şifrə',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
           # 'created_at' => 'Created At',
            #'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartners()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    public function sendEmail($pass)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'new-user-html'],
                ['login' => $this->username,'pass'=>$pass]
            )
            ->setFrom([Yii::$app->params['companyEmail']=> Yii::$app->name])
            ->setTo([$this->email,Yii::$app->params['adminEmail']])
            ->setSubject('Məlumat ' . Yii::$app->name)
            ->send();
    }
}
