<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%discounts}}".
 *
 * @property integer $id
 * @property integer $'partner_id
 * @property double $percent
 * @property string $date
 *
 * @property Partners $partners
 * @property Payments[] $payments
 */
class Discounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%discounts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'percent', 'date'], 'required'],
            [['partner_id'], 'integer'],
            [['percent'], 'number'],
            [['date'], 'safe'],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'Partnyor',
            'percent' => 'Faiz',
            'date' => 'Tarix',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartners()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['discounts_id' => 'id']);
    }
}
