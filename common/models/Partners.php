<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%partners}}".
 *
 * @property integer $id
 * @property string $company
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $contract_date
 * @property boolean $status
 *
 * @property Discounts[] $discounts
 * @property Orders[] $orders
 * @property User[] $users
 */
class Partners extends \yii\db\ActiveRecord
{

    public  $ordersCount;
    public  $ordersWaiting;
    public  $ordersApproved;
    public  $ordersFinished;
    public  $ordersAmount;
    public  $ordersPaid;
    public  $ordersResidual;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%partners}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company', 'email', 'phone', 'address', 'contract_date'], 'required'],
            [['contract_date'], 'safe'],
            [['status'], 'boolean'],
            [['company', 'phone', 'address'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company' => 'Şirkət',
            'email' => 'Email',
            'phone' => 'Telefon',
            'address' => 'Ünvan',
            'contract_date' => 'Müqavilə tarixi',
            'status' => 'Status',
            'ordersCount'       =>  'Ümumi sifariş',
            'ordersWaiting'     =>  'Gözləmədə',
            'ordersApproved'    =>  'Qəbul olunmuş',
            'ordersFinished'    =>  'Bitmiş',
            'ordersAmount'      =>  'Ümumi məbləğ',
            'ordersPaid'        =>  'Ödənilmiş',
            'ordersResidual'    =>  'Qalıq',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(Discounts::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTours()
    {
        return $this->hasMany(Tour::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['partner_id' => 'id']);
    }
}
