<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cars}}".
 *
 * @property integer $id
 * @property integer $car_type_id
 * @property string $driver
 * @property string $car_number
 * @property string $make
 * @property string $model
 * @property string $color
 * @property integer $year
 * @property boolean $status
 *
 * @property CarTypes $carType
 * @property DriverKnowLang[] $driverKnowLangs
 * @property Orders[] $orders
 */
class Cars extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cars}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_type_id', 'driver', 'car_number', 'make', 'model', 'color', 'year', 'phone'], 'required'],
            [['car_type_id', 'year'], 'integer'],
            [['status'], 'boolean'],
            [['driver_gets'], 'number'],
            [['last_payment_date'], 'safe'],
            [['phone'], 'string', 'max' => 50],
            [['driver', 'car_number', 'make', 'model', 'color'], 'string', 'max' => 45],
            [['car_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarTypes::className(), 'targetAttribute' => ['car_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_type_id' => 'Avtomobil tipi',
            'driver' => 'Sürücü',
            'car_number' => 'Avtomobil nömrəsi',
            'make' => 'Marka',
            'model' => 'Model',
            'color' => 'Rəng',
            'year' => 'İl',
            'phone' => 'Telefon',
            'status' => 'Status',
            'driver_gets' => 'Sürücüyə ödənilmiş',
            'last_payment_date' => 'Son ödəniş tarixi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarTypes::className(), ['id' => 'car_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverKnowLangs()
    {
        return $this->hasMany(DriverKnowLang::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['car_id' => 'id']);
    }
}
