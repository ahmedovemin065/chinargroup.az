<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%langs}}".
 *
 * @property integer $id
 * @property string $lang
 * @property string $code
 * @property boolean $status
 *
 * @property DriverKnowLang[] $driverKnowLangs
 */
class Langs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%langs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'code'], 'required'],
            [['status'], 'boolean'],
            [['lang'], 'string', 'max' => 45],
            [['code'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Lang',
            'code' => 'Code',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverKnowLangs()
    {
        return $this->hasMany(DriverKnowLang::className(), ['lang_id' => 'id']);
    }
}
