<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StandardPrices;

/**
 * StandardPricesSearch represents the model behind the search form about `common\models\StandardPrices`.
 */
class StandardPricesSearch extends StandardPrices
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'car_type_id', 'service_id'], 'integer'],
            [['price_partner', 'price_driver'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StandardPrices::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'car_type_id' => $this->car_type_id,
            'service_id' => $this->service_id,
            'price_partner' => $this->price_partner,
            'price_driver' => $this->price_driver,
        ]);

        return $dataProvider;
    }
}
