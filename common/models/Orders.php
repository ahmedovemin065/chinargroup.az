<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%orders}}".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property integer $price_id
 * @property integer $car_id
 * @property string $order_date
 * @property string $created_date
 * @property string $approve_date
 * @property boolean $status
 *
 * @property Cars $car
 * @property Partners $partners
 * @property Prices $prices
 * @property Payments[] $payments
 */
class Orders extends \yii\db\ActiveRecord
{
    public $service_id;
    public $car_type_id;
    public $guest;

    public static $status_type = [1 => "Gözləmədə", 2 => "Qəbul olunmuş", 3 => "Bitmiş"];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'price_id', 'order_date', 'created_date'], 'required'],//, 'approve_date'
            [['tour_id','partner_id' , 'price_id', 'car_id', 'status','hotel_id','extra_day'], 'integer'],
            [['order_date', 'created_date', 'approve_date', 'pay_date'], 'safe'],
            [['paid'], 'boolean'],
            [['price'], 'number'],
            [['ip'], 'string', 'max' => 15],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['hotel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hotels::className(), 'targetAttribute' => ['hotel_id' => 'id']],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['tour_id' => 'id']],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prices::className(), 'targetAttribute' => ['price_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tour_id' => 'Tur',
            'partner_id' => 'Partnyor',
            'price_id' => 'Qiymət',
            'car_id' => 'Avtomobil',
            'order_date' => 'Sifariş Tarixi',
            'created_date' => 'Created Date',
            'approve_date' => 'Approve Date',
            'pay_date' => 'Pay Date',
            'status' => 'Status',
            'paid' => 'Ödəniş',
            'price' => 'Qiymət(AZN)',
            'extra_day' => 'Əlavə gün sayı',
            'hotel_id' => 'Otel',
            'car_type_id'=>'Avtomobil növü',
            'ip' => 'Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */


    public function getTour()
    {

        return $this->hasOne(Tour::className(), ['id' => 'tour_id']);
    }

    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasOne(Prices::className(), ['id' => 'price_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['orders_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotels()
    {
        return $this->hasOne(Hotels::className(), ['id' => 'hotel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }
    
    public static function allStatus()
    {
            return self::$status_type;
    }

    public static function getStatus($key)
    {
        return self::$status_type[$key];
    }

    public function sendEmail()
    {
        $partner = Partners::findOne(Yii::$app->user->identity->partnersid);
        $count_tour = Tour::find()->where(['partner_id'=>Yii::$app->user->identity->partnersid])->count();
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'new-tour-html'],
                ['partner' => $partner,'count'=>$count_tour]
            )
            ->setFrom([Yii::$app->params['companyEmail']=> Yii::$app->name . ' robot'])
            ->setTo([Yii::$app->params['companyincomemail'],Yii::$app->params['adminEmail']])
            ->setSubject('Yeni tur sifarişi ' . Yii::$app->name)
            ->send();
    }


    public static function getOrdersCount($partner_id = null,$type = null){

        if ($partner_id == null){
            if ($type  == null){
                $a = self::find()->where('status > 0')->groupBy('tour_id')->count();
            }else{
                $a = self::find()->where(['status'=>$type])->groupBy('tour_id')->count();
            }
        }else{
            if ($type  == null){
                $a = self::find()->where(['partner_id' => $partner_id])->groupBy('tour_id')->count();
            }else{
                $a = self::find()->where(['partner_id' => $partner_id,'status'=>$type])->groupBy('tour_id')->count();
            }
        }




        return $a;
    }

    public static function getOrdersAmount($partner_id = null,$type = null)
    {
        if ($type == null)
        {
            $where = " O.status > 0 ";
        }else if ($type == 'paid'){
            $where = " O.paid = 1 ";
        }else if($type == 'residual') {
            $where = " O.paid = 0 AND O.status > 0 ";
        }
        if ($partner_id == null){
            $sumprice = Yii::$app->db->createCommand("SELECT
                                                    SUM(O.price) AS lastsum,
                                                    SUM(P.price_partner) AS sumprice,
                                                    SUM(O.extra_day) as sumextraday
                                                FROM
                                                    cms_orders O
                                                        LEFT JOIN
                                                    cms_prices P ON O.price_id = P.id
                                                WHERE
                                                    $where ")->queryOne();

        }else
        {
            $sumprice = Yii::$app->db->createCommand("SELECT
                                                    SUM(O.price) AS lastsum,
                                                    SUM(P.price_partner) AS sumprice,
                                                    SUM(O.extra_day) as sumextraday
                                                FROM
                                                    cms_orders O
                                                        LEFT JOIN
                                                    cms_prices P ON O.price_id = P.id
                                                WHERE
                                                    O.partner_id =:partner_id AND $where ")->bindValue(":partner_id",$partner_id)->queryOne();

        }

        $sp = ($sumprice['lastsum'])?$sumprice['lastsum']:0;

        return Yii::$app->formatter->asCurrency($sp, 'AZN');

    }

    public static function incomeDriver($car_id)
    {
        $driver_income_sum = Yii::$app->db->createCommand("SELECT 
                                                            sum(P.price_driver + (O.extra_day * P.driver_extra_day_price)) as  driver_income_sum
                                                        FROM
                                                            cms_orders O
                                                            INNER JOIN cms_prices P ON O.price_id = P.id
                                                        WHERE
                                                            car_id =:car_id")->bindValue(":car_id",$car_id)->queryScalar();

        return $driver_income_sum;
    }

    public static function ordersCountDriver($car_id)
    {
         $count_order =   Yii::$app->db->createCommand("SELECT count(id) as  count_order FROM  cms_orders O WHERE car_id =:car_id")->bindValue(":car_id",$car_id)->queryScalar();
         return $count_order;
    }




    public static function priceTotal($provider, $fieldName)
    {
        $total=0;
        foreach($provider as $item){
            $total+=$item[$fieldName];
        }
        return $total;
    }

}
