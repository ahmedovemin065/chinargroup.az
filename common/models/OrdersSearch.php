<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'partner_id','tour_id', 'price_id', 'hotel_id', 'car_id', 'extra_day', 'status'], 'integer'],
            [['order_date', 'created_date', 'approve_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find()->andWhere("`status` != 0 AND partner_id =:partner_id" ,[':partner_id'=>Yii::$app->user->identity->partnersid]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'partner_id' => $this->partner_id,
            'price_id' => $this->price_id,
            'tour_id' => $this->tour_id,
            'hotel_id' => $this->hotel_id,
            'car_id' => $this->car_id,
            'extra_day' => $this->extra_day,
            'order_date' => $this->order_date,
            'created_date' => $this->created_date,
            'approve_date' => $this->approve_date,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
