<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tour}}".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property integer $status
 * @property string $create_date
 * @property string $guest
 * @property double $price
 *
 * @property Orders[] $orders
 * @property Partners $partner
 */
class Tour extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tours}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'guest', 'price'], 'required'],
            [['partner_id', 'status'], 'integer'],
            [['create_date'], 'safe'],
            [['price'], 'number'],
            [['paid'], 'boolean'],
            [['guest'], 'string', 'max' => 45],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'Partnyor',
            'status' => 'Status',
            'create_date' => 'Əlavə olunma tarixi',
            'guest' => 'Qonaq',
            'price' => 'Ümumi qiymət(AZN)',
            'paid' => 'Ödəniş',
        ];
    }

    public static function priceTotal($provider, $fieldName)
    {
        $total=0;
        foreach($provider as $item){
            $total+=$item[$fieldName];
        }
        return $total;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['tour_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    public static function tourIncome($tour_id)
    {

        $income = Yii::$app->db->createCommand("SELECT 
                                                (SUM(O.price) -SUM(P.price_driver + (P.driver_extra_day_price * O.extra_day))) as income 
                                            FROM
                                                cms_orders O
                                                    LEFT JOIN
                                                cms_prices P ON O.price_id = P.id
                                            WHERE
                                                O.tour_id =:tour_id")->bindValue(":tour_id",$tour_id)->queryScalar();

        return $income;
    }

    public static function driverIncome($tour_id)
    {

        $income = Yii::$app->db->createCommand("SELECT 
                                                (SUM(P.price_driver + (P.driver_extra_day_price * O.extra_day))) as income 
                                            FROM
                                                cms_orders O
                                                    LEFT JOIN
                                                cms_prices P ON O.price_id = P.id
                                            WHERE
                                                O.tour_id =:tour_id")->bindValue(":tour_id",$tour_id)->queryScalar();

        return $income;
    }

    public static function earn($company = null)
    {
        if ($company == null){
            $income = Yii::$app->db->createCommand("SELECT 
                                                (SUM(O.price) - (SUM(P.price_driver + (P.driver_extra_day_price * O.extra_day)))) as income 
                                            FROM
                                                cms_orders O
                                                    LEFT JOIN
                                                cms_prices P ON O.price_id = P.id AND O.status != 0
                                            ")->queryScalar();
        }else{

            $income = Yii::$app->db->createCommand("SELECT 
                                                (SUM(O.price) - (SUM(P.price_driver + (P.driver_extra_day_price * O.extra_day)))) as income 
                                            FROM
                                                cms_orders O
                                                    LEFT JOIN
                                                cms_prices P ON O.price_id = P.id
                                            WHERE
                                                O.partner_id =:partner_id AND O.status != 0")->bindValue(":partner_id",$company)->queryScalar();
        }



        return Yii::$app->formatter->asCurrency($income, 'AZN');

    }

    public static function expense($company = null)
    {
        if ($company == null){
            $income = Yii::$app->db->createCommand("SELECT 
                                                ((SUM(P.price_driver + (P.driver_extra_day_price * O.extra_day)))) 
                                            FROM
                                                cms_orders O
                                                    LEFT JOIN
                                                cms_prices P ON O.price_id = P.id AND O.status != 0
                                            ")->queryScalar();
        }else{

            $income = Yii::$app->db->createCommand("SELECT 
                                                ((SUM(P.price_driver + (P.driver_extra_day_price * O.extra_day))))
                                            FROM
                                                cms_orders O
                                                    LEFT JOIN
                                                cms_prices P ON O.price_id = P.id
                                            WHERE
                                                O.partner_id =:partner_id AND O.status != 0")->bindValue(":partner_id",$company)->queryScalar();
        }



        return Yii::$app->formatter->asCurrency($income, 'AZN');

    }




}
