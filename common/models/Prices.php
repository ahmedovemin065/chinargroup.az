<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%prices}}".
 *
 * @property integer $id
 * @property integer $car_type_id
 * @property integer $service_id
 * @property integer $'partner_id
 * @property double $price_partner
 * @property double $price_driver
 *
 * @property Orders[] $orders
 * @property CarTypes $carType
 * @property Services $service
 * @property Partners $partners
 */
class Prices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%prices}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_type_id', 'service_id', 'partner_id', 'price_partner', 'price_driver','partner_extra_day_price', 'driver_extra_day_price'], 'required'],
            [['car_type_id', 'service_id', 'partner_id'], 'integer'],
            [['price_partner', 'price_driver','partner_extra_day_price', 'driver_extra_day_price'], 'number'],
            [['service_id'], 'validateDuplicateRow','on' => 'create'],
            [['car_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarTypes::className(), 'targetAttribute' => ['car_type_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    public function validateDuplicateRow($attribute, $params)
    {
        if(!$this->hasErrors()){
            $cnt = self::find()->where(['car_type_id'=>$this->car_type_id,'service_id'=>$this->service_id,'partner_id'=>$this->partner_id])->count();
            if ($cnt > 0){
                $this->addError($attribute, 'İstiqamətə görə standart qiymət təyin olunub');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_type_id' => 'Avtomobil tipi',
            'service_id' => 'İstiqamət',
            'partner_id' => 'Partnyor',
            'price_partner' => 'Partnyor qiyməti',
            'price_driver' => 'Sürücü qiyməti',
            'partner_extra_day_price' => 'Əlavə gün qiyməti(partnyor)',
            'driver_extra_day_price' => 'Əlavə gün qiyməti(sürücü)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['price_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarType()
    {
        return $this->hasOne(CarTypes::className(), ['id' => 'car_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartners()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }
}
