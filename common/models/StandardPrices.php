<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%standard_prices}}".
 *
 * @property integer $id
 * @property integer $car_type_id
 * @property integer $service_id
 * @property double $price_partner
 * @property double $price_driver
 *
 * @property CarTypes $carTypes
 * @property Services $services
 */
class StandardPrices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%standard_prices}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_type_id', 'service_id', 'price_partner', 'price_driver','partner_extra_day_price', 'driver_extra_day_price'], 'required'],
            [['car_type_id', 'service_id'], 'integer'],
            [['service_id'], 'validateDuplicateRow','on' => 'create'],
            [['price_partner', 'price_driver','partner_extra_day_price', 'driver_extra_day_price'], 'number'],
            [['car_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarTypes::className(), 'targetAttribute' => ['car_type_id' => 'id']],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    public function validateDuplicateRow($attribute, $params)
    {
        if(!$this->hasErrors()){
           // $cnt = Yii::$app->db->createCommand("SELECT count(*) FROM `{{standard_prices}}` WHERE  `car_type_id` =:car_type_id AND `service_id` =:service_id ")
               // ->bindValues([':car_type_id'=>$this->car_type_id,':service_id'=>$this->service_id])->queryScalar();
            $cnt = self::find()->where(['car_type_id'=>$this->car_type_id,'service_id'=>$this->service_id])->count();
            if ($cnt > 0){
                $this->addError($attribute, 'İstiqamətə görə standart qiymət təyin olunub');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_type_id' => 'Avtomobil tipi',
            'service_id' => 'İstiqamət',
            'price_partner' => 'Partnyor qiyməti',
            'price_driver' => 'Sürücü qiyməti',
            'partner_extra_day_price' => 'Əlavə gün qiyməti(partnyor)',
            'driver_extra_day_price' => 'Əlavə gün qiyməti(sürücü)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarTypes()
    {
        return $this->hasOne(CarTypes::className(), ['id' => 'car_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasOne(Services::className(), ['id' => 'service_id']);
    }
}
