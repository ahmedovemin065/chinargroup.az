<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Services */

$this->title = 'İstiqaməti yenilə: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'İstiqamətlər', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Yenilə';
?>
<div class="services-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
