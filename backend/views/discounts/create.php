<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Discounts */

$this->title = 'Endirim';
$this->params['breadcrumbs'][] = ['label' => 'Endrim', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discounts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
