<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Discounts */

$this->title = 'Endirim yenilə: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Endirimlər', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Yenilə';
?>
<div class="discounts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
