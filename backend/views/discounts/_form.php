<?php

use common\models\Partners;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Discounts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discounts-form">

    <?php $form = ActiveForm::begin(); ?>

    <? //= $form->field($model, 'partner_id')->textInput() ?>

    <?= $form->field($model, 'partner_id')->dropDownList(
        ArrayHelper::map(Partners::find()->all(), 'id', 'company'),
        ['prompt'=>'----']    // options
    ) ?>


    <?= $form->field($model, 'percent')->textInput() ?>

    <?//= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        //'htmlOtions'=>['class'=>'form-control']
        'options' => ['class' => 'form-control']
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Əlavə et' : 'Yenilə', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
