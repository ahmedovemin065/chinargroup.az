<?php

use common\models\Partners;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DiscountsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Endrimlər';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discounts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Endirim Əlavə et', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'partner_id',
            [
                'attribute' => 'partner_id',
                'value'     => function($dataProvider){
                    return Partners::findOne($dataProvider->partner_id)['company'];//gettype($dataProvider->status);
                },
                'filter'    =>ArrayHelper::map(Partners::find()->asArray()->all(), 'id', 'company'),

            ],
            [
                'attribute' => 'percent',
                'value'     => function($dataProvider){
                    return $dataProvider->percent." %";//gettype($dataProvider->status);
                },

            ],
            //'percent',
            'date',



            [
                'class' => 'yii\grid\ActionColumn',
                'template'  => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
