<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Partners */

$this->title = 'Partnyor yenilə: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Partnyorlar', 'url' => ['index']];

$this->params['breadcrumbs'][] = 'Yenilə';
?>
<div class="partners-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
