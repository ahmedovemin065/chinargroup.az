<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\StandardPricesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Standart qiymətlər';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standard-prices-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Əlavə et', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
//            'car_type_id',
//            'service_id',
//            'price_partner',
//            'price_driver',


        //    ['class' => 'yii\grid\ActionColumn'],

            [
                'attribute' => 'car_type_id',//carType
                'value'     => 'carTypes.title',
                'filter'    =>false
            ],
            [
                'attribute' => 'service_id',//service
                'value'     => 'services.title',
                'filter'    =>false
            ],
            //'price_partner',

            [
                'attribute' => 'price_partner',
                'value'     => function($model){
                    return $model->price_partner." AZN";
                },
            ],

            //'price_driver',

            [
                'attribute' => 'price_driver',
                'value'     => function($model){
                    return $model->price_driver." AZN";
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'  => '{update} {delete}',
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
