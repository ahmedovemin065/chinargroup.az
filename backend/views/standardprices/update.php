<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StandardPrices */

$this->title = 'Standart qiyməti yenilə: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Standart qiymətlər', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Yenilə';
?>
<div class="standard-prices-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
