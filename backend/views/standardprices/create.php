<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StandardPrices */

$this->title = 'Standart qiymət əlavə et';
$this->params['breadcrumbs'][] = ['label' => 'Standart qiymətlər', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standard-prices-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
