<?php

use common\models\Partners;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>
    <?php
    if(!$model->isNewRecord):
    ?>
    <?= Html::checkbox('agree', false, ['label' => 'Yeni şifrə təyin et','id'=>'agree-check']) ?>
    <div style="display: none;" id="password-field">
    <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>
    </div>
    <?php
        else:
    ?>
            <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>
        <?php
        endif;
        ?>
<!--    --><?//= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'status')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList(
        ['0'=>'Deaktiv','10'=>'Aktiv'],
        ['prompt'=>'----']    // options
    ) ?>

<!--    --><?//= $form->field($model, 'created_at')->textInput() ?>

<!--    --><?//= $form->field($model, 'updated_at')->textInput() ?>
<!--    --><?//= $form->field($model, 'partner_id')->textInput() ?>

    <?= $form->field($model, 'partner_id')->dropDownList(
        ArrayHelper::map(Partners::find()->all(), 'id', 'company'),
        ['prompt'=>'----']    // options
    ) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Əlavə et' : 'Yenilə', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$this->registerJs('
$("#agree-check").change(function() {

            if ($(this).is(":checked")) {
                // Do stuff
                $("#password-field").fadeIn("fast");
            }else{
                $("#password-field").fadeOut("fast");
            }
        });
'
);
?>