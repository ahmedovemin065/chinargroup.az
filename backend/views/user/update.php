<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = 'İstifadəçini yenilə: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'İstifadəçilər', 'url' => ['index']];

$this->params['breadcrumbs'][] = 'Yenilə';
?>
<div class="users-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
