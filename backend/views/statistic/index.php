
<h1>Statistika</h1>

<?php

use common\models\Orders;
use common\models\Tour;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PartnersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partners';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-default">
    <div class="panel-heading">
        Partnyorlar
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
<div class="partners-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'company',
            [
                    'attribute' =>  'ordersCount',
                    'value'     => function($model){
                            return Orders::getOrdersCount($model->id);
                    }
            ],
            [
                'attribute' =>  'ordersWaiting',
                'value'     => function($model){
                    return Orders::getOrdersCount($model->id,1);
                }
            ],
            [
                'attribute' =>  'ordersApproved',
                'value'     => function($model){
                    return Orders::getOrdersCount($model->id,2);
                }
            ],
            [
                'attribute' =>  'ordersFinished',
                'value'     => function($model){
                    return Orders::getOrdersCount($model->id,3);
                }
            ],
            [
                'attribute' =>  'ordersAmount',
                'value'     => function($model){
                    return Orders::getOrdersAmount($model->id);
                }
            ],
            [
                'attribute' =>  'ordersAmount',
                'value'     => function($model){
                    return Orders::getOrdersAmount($model->id);
                }
            ],
            [
                'attribute' =>  'ordersPaid',
                'value'     => function($model){
                    return Orders::getOrdersAmount($model->id,'paid');
                }
            ],
            [
                'attribute' =>  'ordersResidual',
                'value'     => function($model){
                    return Orders::getOrdersAmount($model->id,'residual');
                }
            ],
            [
                    'label' =>'Xərc',
                    'value' => function($model){
                            return Tour::expense($model->id);
                    },
                    'format'=>'raw'
            ],
            [
                    'label' =>'Gəlir',
                    'value' => function($model){
                            return Tour::earn($model->id);
                    },
                    'format'=>'raw'
            ]


            // 'contract_date',
            // 'status:boolean',


        ],
    ]); ?>
</div>
</div>
</div>



<div class="panel panel-default">
    <div class="panel-heading">
        Ümumi
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>

                    <th>Ümumi sifarişlər</th>
                    <th>Gözləmədə</th>
                    <th>Qəbul olunmuş</th>
                    <th>Bitmiş</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?=Orders::getOrdersCount()?></td>
                    <td><?=Orders::getOrdersCount(null,1)?></td>
                    <td><?=Orders::getOrdersCount(null,2)?></td>
                    <td><?=Orders::getOrdersCount(null,3)?></td>
                </tr>

                </tbody>
            </table>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>

                    <th class="">Ümumi məbləğ</th>
                    <th class="">Ödənilmiş</th>
                    <th class="">Qalıq</th>
                    <th class="danger">Ümumi xərc</th>
                    <th class="success">Ümumi qazanc</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class=""><?=Orders::getOrdersAmount()?></td>
                    <td class=""><?=Orders::getOrdersAmount(null,'paid')?></td>
                    <td class=""><?=Orders::getOrdersAmount(null,'residual')?></td>
                    <td class="danger"><?=Tour::expense();?></td>
                    <td class="success"><?=Tour::earn();?></td>

                </tr>

                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>