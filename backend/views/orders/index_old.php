<?php

use common\models\Cars;
use common\models\Hotels;
use common\models\Orders;
use common\models\Partners;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bütün sifarişlər';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
<!--        --><?//= Html::a('Create Orders', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'orders_grid']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter' => true,
        'rowOptions' => function ($model) {
            switch ($model->status){
                case 1:
                    return ['class' => 'warning'];//accepted
                    break;
                case 2:
                    return ['class' => 'success'];//
                    break;
                case 3:
                    return ['class' => 'info'];
                    break;
            }
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            //'partner_id',
            [
                'attribute' => 'partner_id',
                'value' => 'partner.company',
                'filter' => ArrayHelper::map(Partners::find()->where(['status'=>1])->asArray()->all(), 'id', 'company'),

            ],
            //'price_id',
            [
                'attribute' => 'İstiqamət',
                'value' => 'prices.service.title',
            ],
            [
                'attribute' => 'Avto növ',
                'value' => 'prices.carType.title',
            ],

            [
                'attribute' => 'hotel_id',
                'value' => 'hotels.title',
                'filter' => ArrayHelper::map(Hotels::find()->where(['status'=>1])->asArray()->all(), 'id', 'title'),
            ],

            [
                'attribute' => 'extra_day',
                'filter' => [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
            ],
            [
                'attribute' => 'Qiymət',
                //'value' => 'prices.price_partner',
                'value' => function($model){
                    return ($model->prices->price_partner + ($model->extra_day*100))." AZN";
                },
                //'footer' => Orders::getTotal($dataProvider->models->price_id, 'price_partner'),

            ],

            //'extra_day',
            'order_date',
            // 'created_date',
            // 'approve_date',

            [
                'attribute' => 'car_id',
                'format' => 'raw',
                //'value' => 'car.driver',
                'value' => function($model){
                    return (isset($model->car->driver))?$model->car->driver:Html::a('Avtomobil ayır', '#', ['class' => 'btn btn-info drivermount','data-id' => $model->id]);//,'data-toggle'=>'modal','data-target'=>'#myModal'
                },
                'filter' => ArrayHelper::map(Cars::find()->where(['status'=>1])->asArray()->all(), 'id', 'driver'),
            ],
            [
                'attribute' => 'paid',
                'value'     => function($dataProvider){
                    if ($dataProvider->status > 1){
                        return ($dataProvider->paid == false)?Html::a('Ödənildi', '#', ['class' => 'btn btn-success pay','data-id' => $dataProvider->id]):'<span class="glyphicon glyphicon-ok" aria-hidden="true" title="Ödəniş olunub"></span>';
                    }else{
                        return '<span class="glyphicon glyphicon-minus" title="Ödəniş oluna bilməz" aria-hidden="true"></span>';
                    }

                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'status',
                'value'     => function($dataProvider){
                    if ($dataProvider->status == 2){
                        return Html::a('Sifariş bitdi', '#', ['class' => 'btn btn-info complateorder','data-id' => $dataProvider->id]);
                    }else{
                        return Orders::getStatus($dataProvider->status);
                    }

                },
                'filter'    =>Orders::allStatus(),
                'format' => 'raw',

            ],
            //'status',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]); ?>

    <?php
    $this->registerJs("
        $(document).ready(function(){
            $('.drivermount').click(function(){
            
                var order_id = $(this).data('id');
                $('body').data( 'order_id', order_id );
                 // submit form
             $.ajax({
                  url: '".Url::to(['orders/getdriver'])."',
                  type: 'GET',
                  data: {'order_id':order_id},
                  dataType: 'HTML',
                  success: function (response) {
                       // do something with response.
                        if (response != ''){
                            $('#myModal .modal-body').html(response);
                            $('#myModal').modal('show');
                        }
                            
                            //$.pjax.reload({container: '#pjax-container'});
                            //$('#myModal').modal('hide');
                            //$('#orders_basket').html(response);                                            
                  }
             });
             return false;
       
               /* $('#myModal').on('shown.bs.modal', function () {
                    //$('#myInput').focus()
                });*/
               
            });
            
            
            $('.save').click(function(){
                               
                var o_id = $('body').data('order_id');
                var d_id = $('#myModal #driverid').val();
                if (d_id != ''){
                    $.ajax({
                      url: '".Url::to(['orders/setdriver'])."',
                      type: 'GET',
                      data: {'order_id':o_id,'driver_id':d_id},
                      dataType: 'HTML',
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                            
                                $('#myModal').modal('hide');
                                $.pjax.reload({container: '#orders_grid'});
                            }                                                                       
                          }
                     });
                }                                                  
            });
            
            
            $('.pay').click(function(){
                var order_id = $(this).data('id');    
                    $.ajax({
                      url: '".Url::to(['orders/pay'])."',
                      type: 'GET',
                      data: {'id':order_id},                      
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                                                          
                                $.pjax.reload({container: '#orders_grid'});
                            }                                                                       
                          }
                     });                                 
            });
            
             $('.complateorder').click(function(){
                var order_id = $(this).data('id');    
                    $.ajax({
                      url: '".Url::to(['orders/complate'])."',
                      type: 'GET',
                      data: {'id':order_id},                      
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                                                          
                                $.pjax.reload({container: '#orders_grid'});
                            }                                                                       
                          }
                     });                                
            });
        });
        
        //setInterval(function(){ $.pjax.reload({container: '#orders'}); }, 10000);
        
");
    ?>
    <?php Pjax::end(); ?></div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sifarişə aid sürücülər</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Bağla</button>
                <button type="button" class="btn btn-primary save">Yadda saxla</button>
            </div>
        </div>
    </div>
</div>





