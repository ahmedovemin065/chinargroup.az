<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <base href="<?= Yii::$app->homeUrl?>">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
       // ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Daxil ol', 'url' => ['/site/login']];
    } else {
        $menuItems =[
            ['label' => 'Əsas səhifə', 'url' => ['/site/index']],
            ['label' => 'Avtomobil', 'url' => '#',
                'items' => [
                        ['label' => 'Avtomobil növləri', 'url' => ['cartypes/index']],
                        ['label' => 'Sürücülər', 'url' => ['cars/index']],
                ],
            ],
            #['label' => 'Avto növlər', 'url' => ['cartypes/index']],
            ['label' => 'Partnyor', 'url' => '#',
                    'items' => [
                        ['label' => 'Partnyorlar', 'url' => ['partners/index']],
                        ['label' => 'Endirimlər', 'url' => ['discounts/index']],
                        ['label' => 'İstifadəçi', 'url' => ['user/index']],
                    ],
            ],

            ['label' => 'Qiymət', 'url' => '#',
                    'items' => [
                        ['label' => 'İstiqamət', 'url' => ['services/index']],
                        ['label' => 'Qiymətlər', 'url' => ['prices/index']],
                        ['label' => 'Standart qiymətlər', 'url' => ['standardprices/index']],
                    ],
            ],
           // ['label' => 'Langs', 'url' => ['langs/index']],

            ['label' => 'Otel', 'url' => ['hotels/index']],
            ['label' => 'Turlar <span></span>', 'format'=>'raw','url' => ['tour/index'],'linkOptions' =>['id'=>'tour_menu']],
            ['label' => 'Statistika', 'url' => ['statistic/index']],
        ];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';

    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
        'encodeLabels' => false,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
    <div class='notifications top-right'></div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>


    </div>
</footer>

<?php $this->endBody() ?>

<?php
$this->registerJs("
    ion.sound({
        sounds: [      
            {name: 'bell_ring'},
        ],
    
        // main config
        path: 'js/sounds/',
       // preload: true,
        volume: 1
    });

     setInterval(function(){ 
                    $.ajax({
                      url: '".Url::to(['tour/notification'])."',
                      type: 'GET',                                     
                      success: function (response) {
                            if (response != ''){    
                                     $('#tour_menu span').text('('+response+')');                                                      
                                    // play sound
                                    ion.sound.play('bell_ring');
                                    // show notification
                                    $('.top-right').notify({
                                        message: {html:  'Yeni <b>'+response+'</b> tur var' },
                                        fadeOut: { enabled: true, delay: 10000 }                                           
                                    }).show();
                                    
                                    $.pjax.reload({container: '#tours_grid'});
                                    
                            }                                                                       
                          }
                     });
     
    }, 1000*10);
  

    
");
?>
</body>
</html>
<?php $this->endPage() ?>
