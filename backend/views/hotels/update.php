<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Hotels */

$this->title = 'Otel yenilə: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Otellər', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Yenilə';
?>
<div class="hotels-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
