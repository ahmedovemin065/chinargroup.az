<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Prices */

$this->title = 'Qiyməti yenilə: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Qiymətlər', 'url' => ['index']];

$this->params['breadcrumbs'][] = 'Yenilə';
?>
<div class="prices-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
