<?php

use common\models\CarTypes;
use common\models\Partners;
use common\models\Services;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Prices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prices-form">

    <?php $form = ActiveForm::begin(); ?>

    <? //= $form->field($model, 'car_type_id')->textInput() ?>
    <?= $form->field($model, 'partner_id')->dropDownList(
        ArrayHelper::map(Partners::find()->all(), 'id', 'company'),
        ['prompt'=>'----']    // options
    ) ?>

    <?= $form->field($model, 'car_type_id')->dropDownList(
        ArrayHelper::map(CarTypes::find()->all(), 'id', 'title'),
        ['prompt'=>'----']    // options
    ) ?>

    <? //= $form->field($model, 'service_id')->textInput() ?>

    <?= $form->field($model, 'service_id')->dropDownList(
        ArrayHelper::map(Services::find()->all(), 'id', 'title'),
        ['prompt'=>'----']    // options
    ) ?>

    <?= $form->field($model, 'price_partner')->textInput(['type'=>'number','step'=>'any']) ?>
    <?= $form->field($model, 'price_driver')->textInput(['type'=>'number','step'=>'any']) ?>
    <?= $form->field($model, 'partner_extra_day_price')->textInput(['type'=>'number','step'=>'any']) ?>
    <?= $form->field($model, 'driver_extra_day_price')->textInput(['type'=>'number','step'=>'any']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Əlavə et' : 'Yenilə', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
