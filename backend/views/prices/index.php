<?php

use common\models\Partners;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PricesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Qiymətlər';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Əlavə et', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Standart qiymətlər', ['standardprices/index'], ['class' => 'btn btn-info']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'car_type_id',

            //'service_id',
            //'partner_id',

            [
                'attribute' => 'partner_id',
                'value'     => function($dataProvider){
                    return Partners::findOne($dataProvider->partner_id)['company'];//gettype($dataProvider->status);
                },
                'filter'    =>ArrayHelper::map(Partners::find()->asArray()->all(), 'id', 'company'),

            ],
            [
                'attribute' => 'car_type_id',//carType
                'value'     => 'carType.title',
                'filter'    =>false
            ],
            [
                'attribute' => 'service_id',//service
                'value'     => 'service.title',
                'filter'    =>false
            ],
            //'price_partner',

            [
                'attribute' => 'price_partner',
                'value'     => function($model){
                    return $model->price_partner." AZN";
                },
            ],

            //'price_driver',

            [
                'attribute' => 'price_driver',
                'value'     => function($model){
                    return $model->price_driver." AZN";
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'  => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
