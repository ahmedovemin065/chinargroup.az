<?php

use common\models\CarTypes;
use common\models\Orders;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CarsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Avtomobil';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cars-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Əlavə et', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(['id' => 'cars_grid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            //'car_type_id',
            [
                'attribute' => 'car_type_id',
                'value'     => function($dataProvider){
                    return CarTypes::findOne($dataProvider->car_type_id)['title'];//gettype($dataProvider->status);
                },
                'filter'    =>ArrayHelper::map(CarTypes::find()->asArray()->all(), 'id', 'title'),
            ],
            'driver',
            'car_number',
           // 'make',
            'phone',
            [
                'label' =>  'Ümumi qazanc',
                'value' =>  function($model){
                        $earn = Orders::incomeDriver($model->id);

                        return ($earn > 0)?Yii::$app->formatter->asCurrency($earn,"AZN"):"-----";
                }
            ],

            [
                    'attribute' =>  'driver_gets',
                    'enableSorting' => false,
                    'value'     =>  function($model){
                       return Yii::$app->formatter->asCurrency($model->driver_gets,"AZN");
                    }
            ],

            [
                'label' =>  'Qalıq',
                'value'     =>  function($model){
                    $income =  Orders::incomeDriver($model->id);
                    $earned = $model->driver_gets;
                    $diff = $income-$earned;
                    return Yii::$app->formatter->asCurrency($diff,"AZN");
                }
            ],

            [
                    'label' =>  'Ödəniş et',
                    'value' =>  function($model){
                        return  Html::a('Ödəniş et', '#', ['class' => 'btn btn-success driver-payment','data-id' => $model->id,'data-toggle'=>'modal','data-target'=>'#myModal']);
                    },
                    'format' => 'raw',
            ],
            // 'year',
            // 'status:boolean',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'  => '{update} {delete}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>


    <?php $form = ActiveForm::begin(); ?>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Sürücüyə ödəniş et</h4>
                </div>
                <div class="modal-body">
                    <?= $form->field($model, 'car_id')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
                        'pluginOptions' => [
                            'autoclose' =>  true,
                            'format'    =>  'yyyy-mm-dd hh:ii',
                            #'startDate' =>  date("Y-m-d H:i"),
                            'language' => 'az',
                        ],
                        'options' => ['class' => 'form-control', 'readonly' => 'true']
                    ]) ?>
                    <?= $form->field($model, 'price')->textInput(['type'=>'number','min'=>'1','step'=>'any']) ?>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Bağla</button>
                    <?= Html::submitButton('Ödəniş et', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

    <?php
    $this->registerJs("
        $(document).on('click','.driver-payment',function() {     
            $('#payments-car_id').val($(this).data('id'));
        });
   
        $('body').on('beforeSubmit', 'form#w1', function () {
             var form = $(this);
             // return false if form still have some validation errors
             if (form.find('.has-error').length) {
                  return false;
             }                                    
             // submit form
             $.ajax({
                  url: form.attr('action'),
                  type: 'post',
                  data: form.serialize(),
                  dataType: 'HTML',
                  success: function (response) {                      
                       if (response == 'success')
                       {                                                                           
                            $('#myModal *').removeClass('has-success');                            
                            $('#myModal input').val('');                                                                                                                         
                            $('#myModal').modal('hide');   
                            $.pjax.reload({container: '#cars_grid'});                                               
                       }
                  }
             });
             return false;
        });

");
    ?>
</div>
