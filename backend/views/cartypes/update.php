<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CarTypes */

$this->title = 'Ban növü yenilə: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ban növləri', 'url' => ['index']];

$this->params['breadcrumbs'][] = 'Yenilə';
?>
<div class="car-types-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
