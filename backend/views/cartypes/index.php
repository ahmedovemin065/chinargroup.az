<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CarTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ban növləri';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-types-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Ban növü əlavə et', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'title',
            'status:boolean',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'  => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
