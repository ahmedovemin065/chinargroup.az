<?php

use common\models\Cars;
use common\models\Orders;
use common\models\Tour;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'summary'=>'Ümumi sifariş sayı {totalCount}',
    'showFooter'    =>  true,
    'footerRowOptions'=>['style'=>'font-weight:bold;'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'prices.service.title',
            'value' => 'prices.service.title',

        ],
        [
            'label' => 'Avtomobil növü',
            'value' => 'prices.carType.title',

        ],

        'extra_day',

        #'car_id',
        'order_date',
        [
            'attribute' => 'hotel_id',
            'value' => 'hotels.title',
        ],

        [
            'attribute' => 'car_id',
            'format' => 'raw',
            //'value' => 'car.driver',
            'value' => function($model){
                return (isset($model->car->driver))?$model->car->driver:Html::a('Avtomobil ayır', '#', ['class' => 'btn btn-info drivermount','data-id' => $model->id,'data-toggle'=>'modal','data-target'=>'#myModal']);//
            },
           // 'filter' => ArrayHelper::map(Cars::find()->where(['status'=>1])->asArray()->all(), 'id', 'driver'),
        ],
        [
            'attribute' => 'price',
            'value'     => function($model){
                return Yii::$app->formatter->asCurrency($model->price, 'AZN');
            },
            'footer'    => Yii::$app->formatter->asCurrency(Tour::priceTotal($dataProvider->models,'price'), 'AZN'),
        ],
        [
            'attribute' => 'prices.price_driver',
            'value'     => function($model){
                return Yii::$app->formatter->asCurrency($model->prices->price_driver + ($model->extra_day *$model->prices->driver_extra_day_price ), 'AZN');
            },
            'footer'    => Yii::$app->formatter->asCurrency(Tour::driverIncome($dataProvider->models[0]['tour_id']), 'AZN'),
        ],
        [
            'label'     => 'Qazanc',
            'value'     => function($model){
                return Yii::$app->formatter->asCurrency(($model->price - $model->prices->price_driver), 'AZN');
            },
            'footer'=> Yii::$app->formatter->asCurrency(Tour::tourIncome($dataProvider->models[0]['tour_id']), 'AZN')
        ],

    ],
]);


?>