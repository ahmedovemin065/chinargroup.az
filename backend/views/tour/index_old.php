<?php

use common\models\Orders;
use common\models\Partners;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TourSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Turlar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(['id' => 'tour_grid']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <!-- <p>
        <?/*= Html::a('Create Tour', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            switch ($model->status){
                case 1:
                    return ['class' => 'warning'];//accepted
                    break;
                case 2:
                    return ['class' => 'success'];//
                    break;
                case 3:
                    return ['class' => 'info'];
                    break;
            }
        },
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\SerialColumn',
                'contentOptions' => ['class' => 'kartik-sheet-style'],
                'width' => '36px',
                'header' => '',
                'headerOptions' => ['class' => 'kartik-sheet-style']
            ],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function ($model, $key, $index, $column) {
                    // return Yii::$app->controller->renderPartial('_expand-row-details', ['model' => $model]);
                },
                'detailUrl'=>\yii\helpers\Url::to(['tour/detail']),
                'headerOptions' => ['class' => 'kartik-sheet-style'] ,
                'expandOneOnly' => true
            ],
            [
                    'attribute' =>  'partner_id',
                    'value'     =>  'partner.company',
                    'filter'    =>  ArrayHelper::map(Partners::find()->where(['status'=>1])->asArray()->all(), 'id', 'company'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'Hamısı'],
                    'filterType' => GridView::FILTER_SELECT2,
            ],


            'create_date',
            'price',

            'guest',
            //'status',
            [
                'attribute' => 'status',
                'value'     => function($dataProvider){
                    if ($dataProvider->status == 2){
                        return Html::a('Sifariş bitdi', '#', ['class' => 'btn btn-info complateorder','data-id' => $dataProvider->id]);
                    }else{
                        return Orders::getStatus($dataProvider->status);
                    }

                },
                'filter'    =>Orders::allStatus(),
                'format' => 'raw',

            ],
            [
                'attribute' => 'paid',
                'value'     => function($dataProvider){
                    if ($dataProvider->status > 1){
                        return ($dataProvider->paid == false)?Html::a('Ödənildi', '#', ['class' => 'btn btn-success pay','data-id' => $dataProvider->id]):'<span class="glyphicon glyphicon-ok" aria-hidden="true" title="Ödəniş olunub"></span>';
                    }else{
                        return '<span class="glyphicon glyphicon-minus" title="Ödəniş oluna bilməz" aria-hidden="true"></span>';
                    }

                },
                'format' => 'raw',
            ],
            //'paid:boolean',
            /*[
                'class' => 'kartik\grid\CheckboxColumn',
                'headerOptions' => ['class' => 'kartik-sheet-style'],
            ],
*/
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],

        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo

        // set your toolbar
        'toolbar' =>  [
            ['content' =>
                Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' =>  'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' =>  'Reset Grid'])
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            //'label'=>'adasdasdasd'
        ],
       // 'exportConfig' => ['pdf','html'],

    ]); ?>

    <?php
    $this->registerJs("
        $(document).ready(function(){
            //$('.drivermount').click(function(){
            $(document).on('click','.drivermount',function() {
                var order_id = $(this).data('id');
                $('body').data( 'order_id', order_id );
                 // submit form
             $.ajax({
                  url: '".Url::to(['orders/getdriver'])."',
                  type: 'GET',
                  data: {'order_id':order_id},
                  dataType: 'HTML',
                  success: function (response) {
                       // do something with response.
                        if (response != ''){
                            $('#myModal .modal-body').html(response);
                            $('#myModal').modal('show');
                        }                                        
                  }
             });
             return false;               
            });
            
            
            $('.save').click(function(){
                               
                var o_id = $('body').data('order_id');
                var d_id = $('#myModal #driverid').val();
                if (d_id != ''){
                    $.ajax({
                      url: '".Url::to(['orders/setdriver'])."',
                      type: 'GET',
                      data: {'order_id':o_id,'driver_id':d_id},
                      dataType: 'HTML',
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                            
                                $('#myModal').modal('hide');
                                $.pjax.reload({container: '#tour_grid'});
                            }                                                                       
                          }
                     });
                }                                                  
            });
            
            
            $('.pay').click(function(){
                var order_id = $(this).data('id');    
                    $.ajax({
                      url: '".Url::to(['orders/pay'])."',
                      type: 'GET',
                      data: {'id':order_id},                      
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                                                          
                                $.pjax.reload({container: '#orders_grid'});
                            }                                                                       
                          }
                     });                                 
            });
            
             $('.complateorder').click(function(){
                var order_id = $(this).data('id');    
                    $.ajax({
                      url: '".Url::to(['orders/complate'])."',
                      type: 'GET',
                      data: {'id':order_id},                      
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                                                          
                                $.pjax.reload({container: '#orders_grid'});
                            }                                                                       
                          }
                     });                                
            });
        });
        
        //setInterval(function(){ $.pjax.reload({container: '#orders'}); }, 10000);
        
");
    ?>
    <?php Pjax::end(); ?>
</div>




<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sifarişə aid sürücülər</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Bağla</button>
                <button type="button" class="btn btn-primary save">Yadda saxla</button>
            </div>
        </div>
    </div>
</div>
