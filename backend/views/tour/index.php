<?php

use common\models\Orders;
use common\models\Partners;
use common\models\Tour;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
//use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TourSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Turlar';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
tr.tour td:first-child {
cursor: pointer;
}
</style>
<div class="tour-index table-responsive">

    <h1><?= Html::encode($this->title) ?></h1>



    <p>
        <?= Html::a('<i class="glyphicon glyphicon-print"></i> Hamısın Çap et', ['tour/export'], ['class' => 'btn btn-warning' ,'target'=>'_blank']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-refresh"></i> Yenilə', '#', ['class' => 'btn btn-warning' ,'onclick'=>"$.pjax.reload({container: '#tours_grid'}); return false;"]) ?>
    </p>
    <?php Pjax::begin(['id' => 'tours_grid']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter'    =>  true,
        'footerRowOptions'=>['style'=>'font-weight:bold;'],
        'rowOptions' => function ($model) {
            switch ($model->status){
                case 1:
                    return ['class' => 'tour warning','data-id'=>$model->id];//accepted
                    break;
                case 2:
                    return ['class' => 'tour success','data-id'=>$model->id];//
                    break;
                case 3:
                    return ['class' => 'tour info','data-id'=>$model->id];
                    break;
            }
        },
        'columns' => [
            [
                'label' => '',
                'value' => function($model){
                    return "<i class='glyphicon glyphicon-expand'  data-id='".$model->id."' ></i>";
                },
                'format' => 'raw',

            ],
            ['class' => 'yii\grid\SerialColumn'],
            [
                    'attribute' =>  'partner_id',
                    'value'     =>  'partner.company',
                    'filter'    =>  ArrayHelper::map(Partners::find()->where(['status'=>1])->asArray()->all(), 'id', 'company'),
            ],

            'create_date',
            [
                    'attribute'=>'price',
                    'value'     =>  function($model){
                        return Yii::$app->formatter->asCurrency($model->price, 'AZN');
                    },
                    'footer'=>Yii::$app->formatter->asCurrency(Tour::priceTotal($dataProvider->models,'price'), 'AZN'),
            ],

            'guest',
            //'status',
            [
                'attribute' => 'status',
                'value'     => function($dataProvider){
                    if ($dataProvider->status == 2){
                        return Html::a('Turu bitdi', 'javascript:void(0)', ['class' => 'btn btn-info complateorder','data-id' => $dataProvider->id]);
                    }else if ($dataProvider->status == 1){
                        return Html::a('Turu qəbul et', 'javascript:void(0)', ['class' => 'btn btn-success allow-tour','data-id' => $dataProvider->id]);
                    }else{
                        return Orders::getStatus($dataProvider->status);
                    }

                },
                'filter'    =>Orders::allStatus(),
                'format' => 'raw',

            ],
            [
                'attribute' => 'paid',
                'value'     => function($dataProvider){
                    if ($dataProvider->status > 0){
                        return ($dataProvider->paid == false)?Html::a('Ödənildi', 'javascript:void(0)', ['class' => 'btn btn-success pay','data-id' => $dataProvider->id]):'<span class="glyphicon glyphicon-ok" aria-hidden="true" title="Ödəniş olunub"></span>';
                    }else{
                        return '<span class="glyphicon glyphicon-minus" title="Ödəniş oluna bilməz" aria-hidden="true"></span>';
                    }

                },
                'format' => 'raw',
            ],
            [
                'label' => 'Çap et',
                'format'=>'raw',
                'value' => function($model){
                    return Html::a('<i class="glyphicon glyphicon-print"></i> Çap et', '#', ['onclick' => "window.open ('".Url::toRoute(['tour/export','id'=>$model->id])."'); return false",'class' => 'btn btn-warning','target'=>'_blank']);
                },
            ],
            //'paid:boolean',
            /*[
                'class' => 'kartik\grid\CheckboxColumn',
                'headerOptions' => ['class' => 'kartik-sheet-style'],
            ],
*/
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],



    ]); ?>

    <?php
    $this->registerJs("
        $(document).ready(function(){
            
            $(document).on('click','.drivermount',function() {
                var order_id = $(this).data('id');
                $('body').data( 'order_id', order_id );
                 // submit form
                 $.ajax({
                      url: '".Url::to(['orders/getdriver'])."',
                      type: 'GET',
                      data: {'order_id':order_id},
                      dataType: 'HTML',
                      success: function (response) {
                           // do something with response.
                            if (response != ''){
                                $('#myModal .modal-body').html(response);
                                $('#myModal').modal('show');
                            }                                        
                      }
                 });
             return false;               
            });
            
            
            $('.save').click(function(){                               
                var o_id = $('body').data('order_id');
                var d_id = $('#myModal #driverid').val();
                var checkb = ($('#myModal #foralldirection').is(':checked'))?1:0;
                if (d_id != ''){
                    $.ajax({
                      url: '".Url::to(['orders/setdriver'])."',
                      type: 'GET',
                      data: {'order_id':o_id,'driver_id':d_id,'driver_for_tour':checkb},
                      dataType: 'HTML',
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                            
                                $('#myModal').modal('hide');
                                //$.pjax.reload({container: '#orders_grid'});
                                
                                $.get({
                                      url: '" . Url::to('tour/detail') . "',
                                      data: { 'id': response},
                                      success: function(data){                    
                                           $('.child-'+response+' td').html(data);
                                      },
                                });
                               
                            }                                                                       
                          }
                     });
                }                                                  
            });
            
            
            $(document).on('click','.pay',function() {                      
                var order_id = $(this).data('id');    
                    $.ajax({
                      url: '".Url::to(['tour/pay'])."',
                      type: 'GET',
                      data: {'id':order_id},                      
                      success: function (response) {                           
                            if (response != ''){                                                          
                                $.pjax.reload({container: '#tours_grid'});                             
                            }                                                                       
                          }
                     });   
                     
                 return false;                              
            });
            
            $(document).on('click','.complateorder',function() {             
                var order_id = $(this).data('id');    
                    $.ajax({
                      url: '".Url::to(['tour/changestatus'])."',
                      type: 'GET',
                      data: {'id':order_id,'type':'complate'},                      
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                                                          
                                $.pjax.reload({container: '#tours_grid'});
                            }                                                                       
                          }
                     });                                
            });  
            $(document).on('click','.allow-tour',function() {             
                var order_id = $(this).data('id');    
                    $.ajax({
                      url: '".Url::to(['tour/changestatus'])."',
                      type: 'GET',
                      data: {'id':order_id,'type':'allow'},                      
                      success: function (response) {
                           // do something with response.
                            if (response != ''){                                                          
                                $.pjax.reload({container: '#tours_grid'});
                            }                                                                       
                          }
                     });  
                     return false;                                
            });
        });
        
        
        
");
    ?>

    <?php
    $this->registerJs('
        $( "#w0 tbody tr.tour td:first-child" ).click(function() {
         var form = $(this);
         var showbtn = form.find("i");  
         var tour_id = showbtn.data("id");
         var cls =  "child-"+tour_id;        
             
       if (form.parent().next().hasClass(cls))
       {      
            if (showbtn.hasClass("glyphicon-collapse-down"))
                showbtn.removeClass("glyphicon-collapse-down").addClass("glyphicon-expand");
            else
                showbtn.removeClass("glyphicon-expand").addClass("glyphicon-collapse-down");    
            
            $("."+cls).toggleClass("hide");
       }else{
            showbtn.removeClass("glyphicon-expand").addClass("glyphicon-collapse-down");
           $.get({
                  url: "' . Url::to('tour/detail') . '",
                  data: { "id": tour_id},
                  success: function(data){                    
                       form.parent().after("<tr class =\""+cls+"\"><td colspan=\"10\">"+data+"</td></tr>"); 
                  },      
            });
       }        
});

'
    );
    ?>

    <?php Pjax::end(); ?>

<?php
$this->registerJs("
           // setInterval(function(){ $.pjax.reload({container: '#tours_grid'}); }, 1000*60);
");
?>
</div>




<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sifarişə aid sürücülər</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Bağla</button>
                <button type="button" class="btn btn-primary save">Yadda saxla</button>
            </div>
        </div>
    </div>
</div>
