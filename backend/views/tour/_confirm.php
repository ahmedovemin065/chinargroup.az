<?php
use yii\helpers\Url;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        Qiymət çap olunsun ?
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">

            <a href="<?=Url::to(['tour/export','id'=>$id,'price'=>'yes'])?>" class="btn btn-primary btn-lg ">Qiymələr çap olunsun</a>
            <a href="<?=Url::to(['tour/export','id'=>$id,'price'=>'no'])?>" class="btn btn-default btn-lg ">Qiymələr çap olunmasın</a>

        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>