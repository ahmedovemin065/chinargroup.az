<?php
use common\models\Orders;
?>

<h1 class="text-center">Turlar</h1>
    <?php
    foreach ($tours as $k => $tour) {
        ?>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Partnyor</th>
                <th>Tur</th>
                <th>Tarix</th>
                <th>Qonaq</th>
                <?php
                if ($price == 'yes'){
                    ?>
                    <th>Qiymət</th>
                    <?php
                }
                ?>

            </tr>
            </thead>
            <tbody>
        <tr>
            <td><?=($k+1)?></td>
            <td><?=$tour->partner->company?></td>
            <td>Tur-<?=$tour->id?></td>

            <td><?=$tour->create_date?></td>
            <td><?=$tour->guest?></td>
            <?php
            if ($price == 'yes'){
                ?>
                <td><?=Yii::$app->formatter->asCurrency($tour->price, 'AZN')?></td>
                <?php
            }
            ?>
        </tr>
        <tr>
            <td colspan="<?=($price == 'yes')?'6':'5'?>">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>İstiqamət</th>
                        <th>Avto ban</th>
                        <th>Əlavə gün</th>
                        <th>Sifariş tarixi</th>
                        <th>Otel</th>
                        <th>Sürücü</th>
                        <th>Avtomobil nömrəsi</th>
                        <?php
                        if ($price == 'yes'){
                            ?>
                            <th>Qiymət</th>
                            <?php
                        }
                        ?>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $orders = Orders::find()->where(['tour_id'=>$tour->id])->all();
                    foreach ($orders as $k => $order) {
                    ?>
                    <tr>
                        <td><?=($k+1)?></td>
                        <td><?=$order->prices->service->title?></td>
                        <td><?=$order->prices->carType->title?></td>
                        <td><?=$order->extra_day?></td>
                        <td><?=$order->order_date?></td>
                        <td><?=$order->hotels->title?></td>
                        <?php
                        if (isset($order->car_id)):
                            ?>
                            <td><?=$order->car->driver.'('.$order->car->phone.')'?></td>
                            <td><?=$order->car->car_number?></td>
                        <?php
                        else:
                            ?>
                            <td>----</td>
                            <td>----</td>
                        <?php
                        endif;
                        ?>
                        <?php
                        if ($price == 'yes'){
                            ?>
                            <td><?=Yii::$app->formatter->asCurrency($order->price, 'AZN')?></td>
                            <?php
                        }
                        ?>
                    </tr>
                <?php
                    }
                    ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
        <?php
    }
    ?>
