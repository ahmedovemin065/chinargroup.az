<?php

namespace backend\controllers;

use Yii;
use common\models\Users;
use common\models\UserSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for Users model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();
        $model->scenario    = Users::SCENARIO_CREATE;
        #$model->status      = 10;
        if ($model->load(Yii::$app->request->post()) && $model->validate() ) {
            $pass = $model->password_hash;
            $model->password_hash   =  Yii::$app->security->generatePasswordHash($model->password_hash);
            $model->auth_key        =  Yii::$app->security->generateRandomString();
            if ($model->save(false)){
                Yii::$app->session->setFlash('success', 'Məlumat uğurla əlavə olundu');
                if ($model->sendEmail($pass))
                    return $this->redirect(['index']);
            }

        }

            return $this->render('create', [
                'model' => $model,
            ]);

    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        #$model->scenario = Users::SCENARIO_UPDATE;
        $old_password = $model->password_hash;

        if ($model->load(Yii::$app->request->post()) ) {
            if (empty($model->password_hash)){
                $model->password_hash = $old_password;
            }else{
                $model->password_hash   =  Yii::$app->security->generatePasswordHash($model->password_hash);
            }
            Yii::$app->session->setFlash('success', 'Məlumat uğurla yeniləndi');
            $model->save();
            return $this->redirect(['index']);
        } else {
            $model->password_hash =  null;
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
