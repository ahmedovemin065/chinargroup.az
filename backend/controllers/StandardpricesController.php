<?php

namespace backend\controllers;

use common\models\Partners;
use common\models\Prices;
use Yii;
use common\models\StandardPrices;
use common\models\StandardPricesSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StandardpricesController implements the CRUD actions for StandardPrices model.
 */
class StandardpricesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StandardPrices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StandardPricesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new StandardPrices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StandardPrices();
        $model->scenario = "create";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Məlumat uğurla əlavə olundu');
            $partners = Partners::find()->where(['status'=>1])->all();
            foreach ($partners as $partner){
                $price = new Prices();
                $price ->car_type_id        = $model->car_type_id;
                $price ->service_id         = $model->service_id;
                $price ->partner_id        = $partner->id;
                $price ->price_partner      = $model->price_partner;
                $price ->price_driver       = $model->price_driver;
                $price ->partner_extra_day_price    = $model->partner_extra_day_price;
                $price ->driver_extra_day_price    = $model->driver_extra_day_price;
                $price->save();

            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StandardPrices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Məlumat uğurla yeniləndi');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StandardPrices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $price = $this->findModel($id);

        //Prices::deleteAll(['car_type_id'=>$price->car_type_id,'service_id'=>$price->service_id]);

        $price ->delete();


        return $this->redirect(['index']);
    }

    /**
     * Finds the StandardPrices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StandardPrices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StandardPrices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
