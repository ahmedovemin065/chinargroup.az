<?php

namespace backend\controllers;

use common\models\Partners;
use common\models\PartnersSearch;
use Yii;
use yii\filters\AccessControl;

class StatisticController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    public function actionIndex()
    {


        $searchModel = new PartnersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
       // $partners = Partners::find()->where(['status'=>1])->all();

        //return $this->render('index');
    }



}
