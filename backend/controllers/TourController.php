<?php

namespace backend\controllers;

use common\models\Orders;
use Yii;
use common\models\Tour;
use backend\models\TourSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TourController implements the CRUD actions for Tour model.
 */
class TourController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tour models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TourSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tour model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tour model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tour();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tour model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDetail($id){
        $tour_id = $id;
        $dataProvider = new ActiveDataProvider([
            'query' => Orders::find()->where(['tour_id'=>$tour_id]) ->orderBy('order_date ASC'),
            'sort' => false,
            //'sort'=> ['defaultOrder' => ['id' => SORT_DESC],'attributes' => []]
        ]);
        return $this->renderPartial('_gettourorders',['dataProvider' => $dataProvider,]);
    }


    public function actionPay($id)
    {
        $order  = $this->findModel($id);
        $order->paid = 1;
        if ($order->save(false)){
            Orders::updateAll(['paid'=>1,'pay_date'=>date("Y-m-d H:i:s")],['tour_id'=>$id]);
            return true;
        }
    }

    public function actionChangestatus($id,$type){
        $order  = $this->findModel($id);
        if ($type == 'allow')
            $order->status = 2;
        else if ($type == 'complate')
            $order->status = 3;

        if ($order->save(false)){
            if ($order->status == 2){
                Orders::updateAll(['status'=>$order->status,'approve_date'=>date("Y-m-d H:i:s")],['tour_id'=>$id]);
            }else{
                Orders::updateAll(['status'=>$order->status],['tour_id'=>$id]);
            }


            return true;
        }
    }

    public function actionExport($id = null,$price =null){

        if ($price == null)
            return $this->render('_confirm',['id'=>$id]);

        if ($id == null)
            $tour  = Tour::find()->where(['status'=>1])->all();
        else
            $tour  = Tour::find()->where(['id'=>$id])->all();

        $content = $this->renderPartial('_export',[ 'tours' => $tour,'price'=>$price]);
        //return $content;
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        return $pdf->render();
    }

    public function actionNotification()
    {
        $new_tour   = Yii::$app->db->createCommand("select count(*) from cms_tours WHERE UNIX_TIMESTAMP(create_date) > (UNIX_TIMESTAMP(now()) - 60) ;")->queryScalar();

        if ($new_tour > 0)
            return $new_tour;
    }


    /**
     * Deletes an existing Tour model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tour model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tour the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tour::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
