<?php

namespace backend\controllers;

use Yii;
use common\models\Orders;
use backend\models\OrdersSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       // $dataProvider->query->andFilterWhere(['status'=>1]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetdriver($order_id){

        $a  = Orders::findOne($order_id);

        $car_type   =  $a->prices->carType->id;
        $order_date =  $a->order_date;

        $cars = Yii::$app->db->createCommand(" SELECT 
                                                id, driver
                                            FROM
                                                cms_cars
                                            WHERE
                                                id NOT IN (SELECT 
                                                        c.id AS busy_cars
                                                    FROM
                                                        cms_cars c
                                                            INNER JOIN
                                                        cms_orders o ON (c.id = o.car_id)
                                                            WHERE DATE(o.order_date) = DATE('".$order_date."')
                                                            AND c.status = 1 AND c.car_type_id =:car_type_id ) AND car_type_id =:car_type_id ;")->bindValue(":car_type_id",$car_type)->queryAll();

       // print_r ($cars);
        if (count($cars) > 0){
            $b = ArrayHelper::map($cars, 'id', 'driver');
            echo Html::dropDownList("cars",0,$b,['prompt'=>'-----','class'=>'form-control','id'=>'driverid']);
            echo '<div class="checkbox">
                      <label>
                       '.Html::checkbox("foralldirection",false,['id'=>'foralldirection']).' Sürücünü tura ayır
                      </label>
                    </div>';

        }else{
            echo "<h3 class='text-center'>Uyğun avtomobil mövcud deyil</h3>";
        }
    }

    public function actionSetdriver($order_id,$driver_id,$driver_for_tour){

        $order  = $this->findModel($order_id);

        if ($driver_for_tour == 1) {
            Yii::$app->db->createCommand("UPDATE cms_orders SET car_id =:driver WHERE id IN (
SELECT
    B.id
FROM
    (
    SELECT
        O.tour_id,
        P.car_type_id,
        O.id
    FROM
        cms_orders O
    INNER JOIN cms_prices P ON
        O.price_id = P.id
) B
INNER JOIN(
    SELECT
        O.tour_id,
        P.car_type_id
    FROM
        cms_orders O
    INNER JOIN cms_prices P ON
        O.price_id = P.id
    WHERE
        O.id =:order
) A
ON
    A.tour_id = B.tour_id AND A.car_type_id = B.car_type_id)")->bindValues([":order"=>$order_id,":driver"=>$driver_id] )->execute();

            return $order->tour_id;
        }else{
            $order->car_id =  $driver_id;
            $order->status = 2;
            if ($order->save(false)){
                //Orders::find()->where(['tour_id'])
                return $order->tour_id;
            }
        }



    }


    public function actionPay($id){
        $order  = $this->findModel($id);
        $order->paid = 1;
        if ($order->save(false)){
            return true;
        }

    }

    public function actionComplate($id){
        $order  = $this->findModel($id);
        $order->status = 3;
        if ($order->save(false)){
            return true;
        }
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
