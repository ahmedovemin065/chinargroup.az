<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center">

        <div><img src="img/cinar-sari-aa_200x220.png" alt="<?= Yii::$app->name ?>"/></div>

        <p class="lead">Müxtəlif növ marka avtomobillərin sifarişi.</p>
        <? if (Yii::$app->user->isGuest) { ?>
            <p><a class="btn btn-lg btn-success" href="<?= Url::to(['site/login']) ?>">Daxil ol</a></p>
        <? } else { ?>
            <a href="<?= Url::to(['tour/index']) ?>" class="btn btn-default btn-lg">Turlar</a>
            <a href="<?= Url::to(['orders/createajax']) ?>" class="btn btn-success btn-lg">Yeni tur yarat</a>
            <?php
        }
        ?>
        <div class="margin-top50">
            <img src="img/front.jpeg" class="img-responsive center-block"/>
        </div>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12 text-center">
                <!--                <h2>Sürətli</h2>-->

                <h4>
                    <p><b>Chinar transport service Ölkə xarici və ölkə daxili daşınma xidmətləri göstərməkdədir.</b></p>
                    <p>
                        Mərkəzləşdirilmiş transport xidməti istənilən növ irili kiçikli müxtəlif model və markalardan
                        ibarət transport vasitələrini korporativ müştərilərinin xidmətinə təqdim edir.Müştərilərin
                        rahatlığını,təhlükəsizliyini təmin etmək üçün şirkətin xüsusi təlimatlardan keçmiş təcrübəli
                        sürücülər ilə texniki baxımdan saz olan 300 dən artıq avtomobillərlə təchiz edilmişdir.</h4>
                </p>

            </div>

        </div>

    </div>
</div>
