<?php

use common\models\Orders;
use common\models\Tour;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TourSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Turlar';
$this->params['breadcrumbs'][] = $this->title;
$partner = Yii::$app->user->identity->partnersid;
?>
<style>
    #w0 table tr.tour td:first-child {
        cursor: pointer;
    }
</style>
<div class="tour-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-ok"></i> Tur əlavə et', ['orders/createajax'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-print"></i> Hamısın Çap et', ['tour/export'], ['class' => 'btn btn-warning' ,'target'=>'_blank']) ?>
    </p>


    <div class="panel panel-default">
        <div class="panel-heading">
            Bütün turlar
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body table-responsive">
<?php Pjax::begin(['id' => 'tours_grid']); ?>
    <?= GridView::widget([
        'dataProvider'  =>  $dataProvider,
        'filterModel'   =>  $searchModel,
        'showFooter'    =>  true,
        'footerRowOptions'=>['style'=>'font-weight:bold;'],
        //'summary' => '',
        'rowOptions' => function ($model) {
            switch ($model->status){
                case 1:
                    return ['class' => 'tour warning' ,'data-id'=>$model->id];//accepted
                    break;
                case 2:
                    return ['class' => 'tour success' ,'data-id'=>$model->id];//
                    break;
                case 3:
                    return ['class' => 'tour info' ,'data-id'=>$model->id];
                    break;
            }
        },
        'columns' => [
            [
                'label' => '',
                'value' => function($model){
                    return "<i class='glyphicon glyphicon-expand'  data-id='".$model->id."' ></i>";
                },
                'format' => 'raw',

            ],

            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'id',
                'value' => function($model){
                    return "Tur-".$model->id;
                },
            ],

            #'partner_id',
            //'status',

            'guest',
            [
                'attribute' => 'price',
                'value' => function($model){
                    return Yii::$app->formatter->asCurrency($model->price, 'AZN');
                },
                'footer'=>Yii::$app->formatter->asCurrency(Tour::priceTotal($dataProvider->models,'price'), 'AZN'),
            ],

            //'price',
            'create_date',
            /*[
                'attribute' => 'create_date',
                'format' => ['date', 'php:d-m-Y H:i:s']
            ],*/

            [
                'attribute' => 'status',
                'value'     => function($dataProvider){
                    return Orders::getStatus($dataProvider->status);//gettype($dataProvider->status);
                },
                'filter'    =>Orders::allStatus(),

            ],
            [
                'label' => 'Çap et',
                'format'=>'raw',
                'value' => function($model){
                    return Html::a('<i class="glyphicon glyphicon-print"></i> Çap et', '#', ['onclick' => "window.open ('".Url::toRoute(['tour/export','id'=>$model->id])."'); return false",'class' => 'btn btn-warning','target'=>'_blank']);
                },
            ],
           // ['class' => 'yii\grid\ActionColumn'],

            [
                'class'     => 'yii\grid\ActionColumn',
                'template'  => '{delete}',

                'buttons' => [
                    'delete' => function($url, $model){
                        if ($model->status == 1){
                            return  Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'title' => 'Sil',
                                'data' => [
                                    'confirm' => 'Silmək istədiyinizdən əminsiniz ?',
                                    'method' => 'post',
                                ],
                            ]);
                        }

                    }
                ]
            ],
        ],
    ]); ?>

    <?php
    $this->registerJs('
        $( "#w0 tbody tr.tour td:first-child" ).click(function() {
         var service = $(this).val(); 
         var form = $(this);
         var showbtn = form.find("i");  
         var tour_id = showbtn.data("id");
         var cls =  "child-"+tour_id;        
             
       if (form.parent().next().hasClass(cls))
       {      
            if (showbtn.hasClass("glyphicon-collapse-down"))
                showbtn.removeClass("glyphicon-collapse-down").addClass("glyphicon-expand");
            else
                showbtn.removeClass("glyphicon-expand").addClass("glyphicon-collapse-down");    
            
            $("."+cls).toggleClass("hide");
       }else{
            showbtn.removeClass("glyphicon-expand").addClass("glyphicon-collapse-down");
           $.get({
          url: "' . Yii::$app->urlManager->createUrl('orders/gettourorders') . '",
          data: { "id": tour_id},
          success: function(data){
             
               form.parent().after("<tr class =\""+cls+"\"><td colspan=\"9\">"+data+"</td></tr>"); 
          },      
        });
       }        
});

'
    );
    ?>
<?php Pjax::end(); ?></div>

    </div>
</div>
<?php
$this->registerJs("
            setInterval(function(){ $.pjax.reload({container: '#tours_grid'}); }, 1000*60);
");
?>

<div class="row">
    <div class="col-md-6">


        <div class="panel panel-default">
            <div class="panel-heading">
                Sifariş sayı
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered">

                        <tr>
                            <th>Ümumi sifarişlər</th>
                            <th>Gözləmədə</th>
                            <th>Qəbul olunmuş</th>
                            <th>Bitmiş</th>
                        </tr>

                        <tr>
                            <td><?=Orders::getOrdersCount($partner)?></td>
                            <td><?=Orders::getOrdersCount($partner,1)?></td>
                            <td><?=Orders::getOrdersCount($partner,2)?></td>
                            <td><?=Orders::getOrdersCount($partner,3)?></td>
                        </tr>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>


    </div>

    <div class="col-md-6">

        <div class="panel panel-default">
            <div class="panel-heading">
                Sifariş məbləğləri
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered">

                        <tr>
                            <th>Ümumi məbləğ</th>
                            <th>Ödənilmiş</th>
                            <th>Qalıq</th>
                        </tr>

                        <tr>
                            <td><?=Orders::getOrdersAmount($partner)?></td>
                            <td><?=Orders::getOrdersAmount($partner,'paid')?></td>
                            <td><?=Orders::getOrdersAmount($partner,'residual')?></td>
                        </tr>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>


    </div>
</div>





