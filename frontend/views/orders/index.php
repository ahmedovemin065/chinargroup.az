<?php


use common\models\Orders;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sifarişlər';
$this->params['breadcrumbs'][] = $this->title;
$partner = Yii::$app->user->identity->partnersid;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php /*// echo $this->render('_search', ['model' => $searchModel]); */?><!--
    --><?/*= $this->render('_form',[
        'model' => $model,
    ]) */?>

    <p>
        <?= Html::a('Yeni sifariş ver', ['createajax'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showFooter'=>true,
        'rowOptions' => function ($model) {
            switch ($model->status){
                case 1:
                    return ['class' => 'warning'];//accepted
                    break;
                case 2:
                    return ['class' => 'success'];//
                    break;
                case 3:
                    return ['class' => 'info'];
                    break;
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            //'tour_id',
            [
                'attribute' => 'tour_id',
                'value' => function($model){
                        return "Tur-".$model->tour->id;
                },
                //'group'=>true,
            ],
            [
                'attribute' => 'tour.guest',
                'value' => function($model){
                        return $model->tour->guest;
                },
            ],
            [
                'attribute' => 'tour.price',
                'value' => function($model){
                    return $model->tour->price;
                },
            ],
            /*[
                'attribute' => 'partners',
                'value' => 'partners.company',
            ],*/
            //'price_id',
            [
                'attribute' => 'prices.service.title',
                'value' => 'prices.service.title',
            ],
            [
                'attribute' => 'prices.carType.title',
                'value' => 'prices.carType.title',
            ],
            'extra_day',
            [
                'attribute' => 'price',
                'value' => function($model){
                    return $model->price." AZN";
                },
            ],
           /* [
                'label' => 'Qiymət',
                //'value' => 'prices.price_partner',
                'value' => function($model){
                    return ($model->prices->price_partner + ($model->extra_day*100))." AZN";
                },
                'footer' => $sumprice." AZN",
            ],*/


            #'car_id',
            'order_date',
            [
                'attribute' => 'hotel_id',
                //'value' => 'hotels.title',
                'value' =>  function($model){
                    return (isset($model->hotel_id) )?$model->hotels->title:"------";
                },
            ],


            //'hotel_id',
            //'car_id',

            // 'created_date',
            // 'approve_date',
            //'status',
            [
                'attribute' => 'status',
                'value'     => function($dataProvider){
                    return Orders::getStatus($dataProvider->status);//gettype($dataProvider->status);
                },
                'filter'    =>Orders::allStatus(),

            ],

            /*[
                'class'     => 'yii\grid\ActionColumn',
                'template'  => '{delete} {update}',

               'buttons' => [
                    'update' => function ($url, $model, $key) {
                                return $model->status == 0 ? Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url) : '';
                    }
                    ,'delete' => function ($url, $model, $key) {
                                return $model->status == 0 ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url) : '';
                    }
                ]
            ],*/
        ],
    ]); ?>
    <?php Pjax::end(); ?>

    <div class="row">
        <div class="col-md-6">


            <div class="panel panel-default">
                <div class="panel-heading">
                    Sifariş sayı
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">

                            <tr>
                                <th>Ümumi sifarişlər</th>
                                <th>Gözləmədə</th>
                                <th>Qəbul olunmuş</th>
                                <th>Bitmiş</th>
                            </tr>

                            <tr>
                                <td><?=Orders::getOrdersCount($partner)?></td>
                                <td><?=Orders::getOrdersCount($partner,1)?></td>
                                <td><?=Orders::getOrdersCount($partner,2)?></td>
                                <td><?=Orders::getOrdersCount($partner,3)?></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>


        </div>

        <div class="col-md-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    Sifariş məbləğləri
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">

                            <tr>
                                <th>Ümumi məbləğ</th>
                                <th>Ödənilmiş</th>
                                <th>Qalıq</th>
                            </tr>

                            <tr>
                                <td><?=Orders::getOrdersAmount($partner)?></td>
                                <td><?=Orders::getOrdersAmount($partner,'paid')?></td>
                                <td><?=Orders::getOrdersAmount($partner,'residual')?></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>


        </div>
    </div>
</div>
<?php
/*
// You only need add this,
$this->registerJs('
        var gridview_id = "#w0"; // specific gridview
        var columns = [2,3,4]; // index column that will grouping, start 1  
        var column_data = [];
            column_start = [];
            rowspan = [];
 
        for (var i = 0; i < columns.length; i++) {
            column = columns[i];
            column_data[column] = "";
            column_start[column] = null;
            rowspan[column] = 1;
        }
 
        var row = 1;
        $(gridview_id+" table > tbody  > tr").each(function() {
            var col = 1;
            $(this).find("td").each(function(){
                for (var i = 0; i < columns.length; i++) {
                    if(col==columns[i]){
                        if(column_data[columns[i]] == $(this).html()){
                            $(this).remove();
                            rowspan[columns[i]]++;
                            $(column_start[columns[i]]).attr("rowspan",rowspan[columns[i]]);
                        }
                        else{
                            column_data[columns[i]] = $(this).html();
                            rowspan[columns[i]] = 1;
                            column_start[columns[i]] = $(this);
                        }
                    }
                }
                col++;
            })
            row++;
        });
    ');*/
?>