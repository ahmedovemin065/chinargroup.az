<?php
use common\models\CarTypes;
use common\models\Hotels;
use common\models\Services;
use kartik\datetime\DateTimePicker;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = 'Yeni tur yarat';
$this->params['breadcrumbs'][] = ['label' => 'Turlar', 'url' => ['tour/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-create">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><?= Html::encode($this->title) ?></h4>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body ">

            <div class="orders-form">

                <?php $form = ActiveForm::begin(['enableAjaxValidation' => true, 'validationUrl' => Url::toRoute('orders/validate')]); ?>
                <?= $form->errorSummary($model); ?>
                <!--    --><? //= $form->field($model, 'partner_id')->textInput() ?>

                <?= $form->field($model, 'car_type_id')->dropDownList(
                    ArrayHelper::map(CarTypes::find()->where(['status' => '1'])->all(), 'id', 'title'),
                    [
                        'prompt' => '----',
                    ]    // options
                ) ?>
                <div id="response_table"></div>



                <? //= $form->field($model, 'created_date')->textInput() ?>

                <? //= $form->field($model, 'approve_date')->textInput() ?>

                <? //= $form->field($model, 'status')->checkbox() ?>


                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Səbətə əlavə et</h4>
                            </div>
                            <div class="modal-body">

                                <?= $form->field($model, 'price_id')->hiddenInput()->label(false) ?>

                                <?= $form->field($model, 'hotel_id')->dropDownList(
                                    ArrayHelper::map(Hotels::find()->all(), 'id', 'title'),
                                    [
                                        'prompt'=>'----',
                                        'class'=>'form-control selectpicker'
                                    ]    // options
                                )->label(null,['data-toggle'=>'tooltip','title'=>'Əgər Qonaq otelə gedəcəksə otel siyahıdan düzgün seçilməlidir','data-placement'=>'right']) ?>
                                <div class="alert alert-info">
                                    Əgər <strong>Qonaq</strong> otelə gedəcəksə otel siyahıdan düzgün seçilməlidir
                                </div>


                                <?= $form->field($model, 'extra_day')->textInput(['type' => 'number','min'=>0,'value'=>0])
                                    ->label(null,['data-toggle'=>'tooltip','title'=>'Qonağın cari turada qalacağı gün sayı qeyd olunmalıdır','data-placement'=>'right']) ?>
                                <div class="alert alert-info">
                                    <strong>Qonağın</strong> cari turada qalacağı gün sayı qeyd olunmalıdır
                                </div>
                                <? //= $form->field($model, 'car_id')->textInput() ?>

                                <? //= $form->field($model, 'order_date')->textInput() ?>

                                <?= $form->field($model, 'order_date')->widget(DateTimePicker::classname(), [
                                    //'language' => 'ru',
                                    // 'dateFormat' => 'yyyy-MM-dd  hh:ii',
                                    'pluginOptions' => [
                                        'autoclose' =>  true,
                                        'format'    =>  'yyyy-mm-dd hh:ii',
                                        'startDate' =>  date("Y-m-d H:i"),
                                        #'todayHighlight' => true,
                                        'language' => 'az',

                                    ],
                                    //'htmlOtions'=>['class'=>'form-control']
                                    'options' => ['class' => 'form-control', 'readonly' => 'true']
                                ]) ?>
                                <div class="alert alert-info">
                                    Sifarişin tarixi dəqiq qeyd olunmalıdır
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Bağla</button>
                                <?= Html::submitButton($model->isNewRecord ? 'Əlavə et' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <?php ActiveForm::end(); ?>

                <?php $form1 = ActiveForm::begin(); ?>
                <div class="modal fade" id="myModalGuestName" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Qonağın adını daxil et</h4>
                            </div>
                            <div class="modal-body">
                            <?= $form1->field($modeltour, 'guest')->textInput() ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Bağla</button>
                                <?= Html::submitButton($model->isNewRecord ? 'Göndər' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <?php ActiveForm::end(); ?>


            </div>

            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>



    <?php Pjax::begin(['id' => 'pjax-container']); ?>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>

        <a href="<?=Url::to(['tour/index'])?>" class="btn btn-info btn-lg"><i class="glyphicon glyphicon-menu-left"></i> Sifarişlərim</a>
    <?php endif; ?>

    <?
    if ($sumprice['price_without_discount'] > 0):
    ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Səbət (Hal-hazırda səbətdə <b><?=$inbasket?></b> sifariş var)</h3>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
//            'partner_id',
            /*[
                'attribute' => 'partners',
                'value' => 'partners.company',
            ],*/
            //'price_id',
            [
                'label' => 'İstiqamət',
                'value' => 'prices.service.title',
            ],
            [
                'label' => 'Avtomobil növü',
                'value' => 'prices.carType.title',
            ],
            [
                'label' => 'Qiymət',
                //'value' => 'prices.price_partner',
                'value' => function($model){
                    return Yii::$app->formatter->asCurrency(($model->prices->price_partner + ($model->extra_day*$model->prices->partner_extra_day_price)), 'AZN');
                },
                //'footer' => $sumprice,
            ],
            'extra_day',

            [
                'label' => 'Əlavə gün qiyməti',
                //'attribute' => 'prices.partner_extra_day_price',
                //'value' => 'prices.price_partner',
                'value' => function($model){
                    return Yii::$app->formatter->asCurrency($model->prices->partner_extra_day_price, 'AZN');
                },
                //'footer' => $sumprice,
            ],
            #'car_id',
            'order_date',
           /* [
                'attribute' => 'order_date',
                'format' => ['datetime', 'php:d-m-Y H:i:s']
            ],*/
            [
                'attribute' => 'hotel_id',
                'value' => 'hotels.title',
            ],

            //'hotel_id',
            //'car_id',
            // 'extra_day',
            // 'created_date',
            // 'approve_date',
            //'status',
            /*[
                'attribute' => 'status',
                'value'     => function($dataProvider){
                    return Orders::getStatus($dataProvider->status);//gettype($dataProvider->status);
                },
                'filter'    =>Orders::allStatus(),

            ],*/

            [
                'class'     => 'yii\grid\ActionColumn',
                'template'  => '{delete}',
                'buttons' => [
                    'delete' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'onclick' => "
                                if (confirm('".Yii::t('yii', 'Are you sure you want to delete this item?')."')) {
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#pjax-container'});
                                    });
                                }
                                return false;
                            ",
                        ]);
                    },
                ],
                // 'visible'   => '(status == 0)?true:false'

            ],
        ],
    ]); ?>

        </div>
        </div>


    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered" id="myTable">

                <tr>
                    <th>Qiymət</th>
                    <th>Endirim</th>
                    <th>Yekun qiymət</th>
                    <td rowspan="2">
                        <a href="#" data-toggle="modal" data-target="#myModalGuestName" class="btn btn-success">GÖNDƏR</a>
                    </td>
                </tr>
                <tr>
                    <td><?=Yii::$app->formatter->asCurrency($sumprice['price_without_discount'], 'AZN')?></td>
                    <td><b><?=(int) $discount['percent']?> %</b> </td>
                    <td><?=Yii::$app->formatter->asCurrency( $sumprice['price_with_discount'], 'AZN')?></td>

                </tr>

            </table>

        </div>
    </div>

    <?
    endif;
    ?>
    <?php Pjax::end(); ?>

    <?php
    $this->registerJs('
        $( "#orders-car_type_id" ).change(function() {
         var service = $(this).val();          
            $.get({
              url: "' . Yii::$app->urlManager->createUrl('orders/pricebyclass') . '",
              data: { "id": service},
              success: function(data){
                   $("#response_table").html(data); 
              },
              //dataType: dataType
            });
        });
         $(\'[data-toggle="tooltip"]\').tooltip();
        ');
    ?>
    <?php
    $this->registerJs("
            
        $('#myModal').on('shown.bs.modal', function () {
                
        });
   
        $('body').on('beforeSubmit', 'form#w0,form#w1', function () {
             var form = $(this);
             // return false if form still have some validation errors
             if (form.find('.has-error').length) {
                  return false;
             }                       
             
             // submit form
             $.ajax({
                  url: form.attr('action'),
                  type: 'post',
                  data: form.serialize(),
                  dataType: 'HTML',
                  success: function (response) {
                       // do something with response.
                       if (response == 'success')
                       {    
                            $('html, body').animate({ scrollTop:  $('#pjax-container').offset().top - 50 }, 'slow');
                            $('.selectpicker').val('default');                            
                            $('.selectpicker').selectpicker('refresh'); 
                                                       
                            $('#myModal *,#myModalGuestName *').removeClass('has-success');
                            
                            $('#myModal input[type=text],#myModal select').val('');
                            $('#myModalGuestName input[type=text]').val('');
                            
                            $.pjax.reload({container: '#pjax-container'});
                            $('#myModal').modal('hide');                            
                            $('#myModalGuestName').modal('hide');                            
                       }
                  }
             });
             return false;
        });
        

        $('select').selectpicker({
            liveSearch: true,
            //maxOptions: 1,
            size: 10
        });
");
    ?>
</div>





