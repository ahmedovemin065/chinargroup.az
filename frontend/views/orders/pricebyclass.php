<?php
/**
 * Created by PhpStorm.
 * User: Emin
 * Date: 11/15/2017
 * Time: 17:12
 */

//print_r($res);


?>

<table class="table table-bordered" id="myTable">
    <thead>
    <tr>
        <th>#</th>
        <th>İstiqamət</th>
        <th>Qiymət</th>
        <th class="hide">Seçim</th>
        <th>Əlavə gün qiyməti</th>
        <th></th>

    </tr>
    </thead>
    <tbody>
    <?foreach ($res as  $k => $row):?>
    <tr class="clickable-row">

        <th scope="row"><?=($k+1)?></th>
        <td><?=$row['title']?></td>
        <td><?=Yii::$app->formatter->asCurrency($row['price_partner'], 'AZN')?></td>
        <td class="hide"><?= \yii\bootstrap\Html::radio('price_id',false,['value'=>$row['id']])?></td>
        <td><?=Yii::$app->formatter->asCurrency($row['partner_extra_day_price'], 'AZN');?></td>
        <td><button type="button" class="btn btn-success" data-toggle="modal" data-id="<?=$row['id']?>" data-target="#myModal">Səbətə at</button></td>

    </tr>
    <?endforeach;?>
    </tbody>
</table>

<script type="text/javascript">
    $('#myTable').on('click', '.clickable-row', function(event) {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        } else {
            $(this).addClass('active').siblings().removeClass('active');
            $("input[type=radio]").prop("checked",false);
            $(this).find("input[type=radio]").prop("checked",true);
            $("#orders-price_id").val($(this).find("input[type=radio]").val());
        }
    });
</script>

