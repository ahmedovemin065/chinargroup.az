<?php

use common\models\Hotels;
use common\models\Services;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
//echo Yii::$app->user->partner_id;
?>

<?php
$this->registerJs(
    '$("document").ready(function(){
            $("#new_order").on("pjax:end", function() {
            $.pjax.reload({container:"#orders"});  //Reload GridView
        });
    });'
);
?>
<div class="orders-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
    <? //= $form->errorSummary($model); ?>
<!--    --><?//= $form->field($model, 'partner_id')->textInput() ?>

    <?= $form->field($model, 'service_id')->dropDownList(
        ArrayHelper::map(Services::find()->all(), 'id', 'title'),
        [
                'prompt'=>'----',
        ]    // options
    ) ?>
    <div id="response_table"></div>
    <?= $form->field($model, 'price_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'hotel_id')->dropDownList(
        ArrayHelper::map(Hotels::find()->all(), 'id', 'title'),
        [
            'prompt'=>'----',
        ]    // options
    ) ?>
    <? //= $form->field($model, 'car_id')->textInput() ?>
    <?= $form->field($model, 'extra_day')->textInput() ?>

    <!--    --><?//= $form->field($model, 'car_id')->textInput() ?>

<!--    --><?//= $form->field($model, 'order_date')->textInput() ?>

    <?= $form->field($model, 'order_date')->widget(DateTimePicker::classname(), [
        //'language' => 'ru',
       // 'dateFormat' => 'yyyy-MM-dd  hh:ii',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd hh:ii',

        ],
        //'htmlOtions'=>['class'=>'form-control']
        'options' => ['class' => 'form-control','readonly'=>'true']
    ]) ?>



<!--    --><?//= $form->field($model, 'created_date')->textInput() ?>

<!--    --><?//= $form->field($model, 'approve_date')->textInput() ?>

<!--    --><?//= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>


<?php
 $this->registerJs('



$( "#orders-service_id" ).change(function() {
 var service = $(this).val();
  
    $.get({
      url: "'.Yii::$app->urlManager->createUrl('orders/pricebyclass').'",
      data: { "id": service},
      success: function(data){
           $("#response_table").html(data); 
      },
      //dataType: dataType
    });
});

'
);
?>