<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'summary'=>'Ümumi sifariş sayı {totalCount}',

    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'prices.service.title',
            'value' => 'prices.service.title',

        ],
        [
            'label' => 'Avtomobil növü',
            'value' => 'prices.carType.title',

        ],
        [
            'attribute' =>'car.model',
            'value' =>function($model){
                return (!empty($model->car_id))?$model->car->make.' '.$model->car->model:'----';
            }
        ],
        [
            'attribute' => 'price',
            'value'     => function($model){
                return Yii::$app->formatter->asCurrency($model->price, 'AZN');
            },
        ],

        [
            'attribute'=>'extra_day',
            //'contentOptions'=>['width'=>'80']
        ],



        [
            'label' =>'Əlavə günün qiyməti',
            'value' =>function($model){
                    return Yii::$app->formatter->asCurrency($model->prices->partner_extra_day_price * $model->extra_day, 'AZN');
            }
        ],



        'order_date',
        [
            'attribute' => 'hotel_id',
            'value' => 'hotels.title',
        ],

        [
            'attribute' => 'car_id',
            'value'     => function($model){
                return (!empty($model->car_id))?$model->car->driver."(".$model->car->phone.")":"----";
            }
        ],
        [
            'label' => 'Nömrə',
            'value'     => function($model){
                return (!empty($model->car_id))?$model->car->car_number:"----";
            },

        ]


    ],
]); ?>
