<?php

namespace frontend\models;

use Yii;


/**
 * This is the model class for table "{{%orders}}".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property integer $price_id
 * @property integer $car_id
 * @property string $order_date
 * @property string $created_date
 * @property string $approve_date
 * @property boolean $status
 *
 * @property Cars $car
 * @property Partners $partners
 * @property Prices $prices
 * @property Payments[] $payments
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'price_id', 'order_date', 'created_date', 'approve_date'], 'required'],
            [['partner_id', 'price_id', 'car_id'], 'integer'],
            [['order_date', 'created_date', 'approve_date'], 'safe'],
            [['status'], 'boolean'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prices::className(), 'targetAttribute' => ['price_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'Partners ID',
            'price_id' => 'Prices ID',
            'car_id' => 'Car ID',
            'order_date' => 'Order Date',
            'created_date' => 'Created Date',
            'approve_date' => 'Approve Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartners()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasOne(Prices::className(), ['id' => 'price_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['orders_id' => 'id']);
    }
}
