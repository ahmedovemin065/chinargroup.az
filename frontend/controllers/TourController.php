<?php

namespace frontend\controllers;

use Yii;
use common\models\Tour;
use common\models\TourSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TourController implements the CRUD actions for Tour model.
 */
class TourController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index','export','delete','importhotel'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tour models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TourSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExport($id = null,$price = null)
    {

        if ($price == null){
            return $this->render('_confirm',['id'=>$id]);
        }
        $partner_id = Yii::$app->user->identity->partnersid;
        if ($id == null) {
            $tour = Tour::find()->where(['partner_id' => $partner_id])->all();//'status' => 2,
        } else {
            $tour = Tour::find()->where(['partner_id' => $partner_id, 'id' => $id])->all();//'status' => 2,
        }

        if (empty($tour)) {
            return $this->redirect(['tour/index']);
            exit;
        }

        $content = $this->renderPartial('_export', ['tours' => $tour,'price'=>$price]);
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        return $pdf->render();
    }

      /**
     * Deletes an existing Tour model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Tour model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tour the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tour::findOne(['id'=>$id,'status'=>1,'partner_id'=>Yii::$app->user->identity->partnersid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
/*
    public function actionImporthotel(){
        $str = "Anatolia Hotel,Miraj İnn Boutique Hotel,Qorqud Hotel,Arriva Hotel,IVA Hotel,Red Roof Hotel,Ramada Baku,Four Seasons Hotel Baku,Smith Hotel,Austin Hotel Baku,Ramada Hotel & Suites Baku,Jasmine Hotel,Pullman Baku,Fairmont Baku,Flame Towers,Boyuk Gala Hotel,Hale Kai Hotel,Excelsior Hotel & Spa Baku,Baku Palace Hotel,Diplomat Hotel,Staybridge Suites Baku,Central Park Hotel,Old East Hotel,Gəncəli Plaza Baku,Holiday İnn Baku,AYF Palace,Riviera Hotel,Qafqaz Park Hotel,Sun Rise Hotel,Karat İnn Hotel,Passage Boutique Hotel,Du Port Hotel,Astoria Baku Hotel,BP Hotel Baku,İron Hotel,Hotel İrshad, AEF Hotel, JW Marriott Absheron Hotel,Hyatt Regency Baku ,Europe Hotel,Hilton Hotel Baku,Park İnn Hotel,Premier Hotel,New Baku Hotel ,Qafqaz Hotel,Landmark Hotel ,Qafqaz Point Boutique Hotel,Ariva Hotel,Avand Hotel,Safran Hotel,İntourist Hotel Baku,Amber Hotel,Sapphire City Hotel,Shah Palace Hotel,Caspian Palace Hotel,Teatro Boutique Hotel,Chiraq Plaza Hotel,Bosfor Hotel Baku, Planet İnn Hotel Baku,Days Hotel Baku,Boulevard Hotel Baku, Intourist Hotel,Consul Hotel,Sapphire İnn Hotel,Nemi Hotel,Royal Hotel,Briston Hotel,Divan Express Hotel,Egoist Hotel,Winter Park Hotel,Jireh Hotel,Azcot Hotel,Old Baku Hotel,Lake Palace ,Askar Hotel,Old Gates Hotel,Renaissance Palace Baku,Empire Hotel,Boutique Hotel Baku,Riva Hotel Baku,Atfk Hotel,Travel Hotel,Sultan İnn Boutique Hotel,East Legent Panorama Hotel,Boulevard Side Hotel,Rich Hotel Baku,Paradise Hotel Baku,Horizon Hotel,Vania Hotel,The Crown Hotel,Swan Hotel Baku,Old street Boutique,Grand Hotel,Caspian Hotel,Seven Rooms Hotel,Emerald Hotel Baku,La Villa Hotel,Boutique Palace,Golden Coast,Don-Dar Hotel,Caspian Business Hotel,Balion Hotel,İva Hotel ,Terrace Hotel,Museum İnn,Garden Villa Hotel,Clover Hotel,Xudaferin Hotel,Rigs Hotel ,Old City İnn ,All Seasons Hotel ,Nur-2 Hotel ,Comfort İnn ,Garden City Hotel ,Xan Çinar Hotel ,İstabul Hotel ,Teymur Mini Hotel ,12 İnn Bulvar ,Avesta Hotel ,Black Mount Hotel ,City Walls Hotel,Homebridge Hotel ,Galery Hotel Baku,History Boutique Hotel,Abu Turan Hotel,Hotel Sea Port Hotel ,Radisson Sas Plaza Hotel ,Deniz İnn Boutique Hotel,Rumya Hotel ,Titanik Hotel,Avenue Hotel,Bayil İnn Hotel,Viva Boutique Hotel,Two Seasons Boutique Hotel,Sapphire Marine Hotel,Gallery Hotel Baku,Atropat Hotel,Gallery Lux,Serin Hotel,Monolit Plaza,Continent Hotel,Deluxe City Hotel,Nord West Hotel,Aysberq Resort,Nika Hotel,Life Holiday,Clear Hotel,Delfin Hotel,Ruma Port Hotel ,Elite Hotel ,KBT Boutique Hotel,Hotel Baku Comfort Rooms,Neapol Hotel,Shams Hotel,Joyy Hotel,Modern Hotel,Azalea Hotel Baku,City Palace Hotel,Rixos Quba Azerbaijan, Qafqaz Karvansaray Hotel, Qafqaz Sport Hotel, QafqaZ Qabala City Hotel, Qəbələ şəhər oteli, Qafqaz Resort Hotel, Qafqaz Tufandag Mountain Resort Hotel, Qafqaz Riverside Resort Hotel, Elelef park hotel, Nohur Hotel, Gilan Hotel, Qafqaz Yeddi Gozel Hotel, Terrace Hotel Quba, Shane Hotel Quba, Rixos Quba Azerbaijan, Cənnət Bağı Quba istirahət mərkəzi, Oskar Hotel,Shahdag Guba Hotel & Spa, MestDergah, Bərpa SPA Mərkəzi, \"Quba\" İstirahət Mərkəzi, Aynur Hotel&Restaurant, Quba Qechresh Holiday House, Quba Retro Hotel, Qusar Olimpik Hotel, Ladera Resort, Zirve Hotel Shahdag, Şahdağ Turizm Kompleksi, Shahdag Hotel & Spa, Butik hotel RA, P.I.K. Palace";
        $ex = explode(',',$str);

        foreach ($ex as $row){
            $otel = new Hotels();
            $otel->title = htmlspecialchars(trim($row));
            $otel->status = 1;
            $otel->save();
        }

    }*/
}
