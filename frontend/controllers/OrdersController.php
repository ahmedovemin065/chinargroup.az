<?php

namespace frontend\controllers;

use common\models\Prices;
use common\models\Tour;
use Yii;
use common\models\Discounts;
use common\models\Orders;
use common\models\OrdersSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['gettourorders', 'createajax','delete','validate','pricebyclass'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            [
                'class' => 'yii\filters\AjaxFilter',
                'only' => ['gettourorders','validate','pricebyclass']
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        //echo Yii::$app->user->identity->partnersid;
        //exit;

        $model = new Orders();
        $model->partner_id     =   Yii::$app->user->identity->partnersid;
        $model->created_date    =   date("Y-m-d H:i:s");

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $model = new Orders();

        }
        $sumprice = Yii::$app->db->createCommand("SELECT
                                                    SUM(P.price_partner) AS sumprice,
                                                    SUM(O.extra_day) as sumextraday
                                                FROM
                                                    cms_orders O
                                                        LEFT JOIN
                                                    cms_prices P ON O.price_id = P.id
                                                WHERE
                                                    O.partner_id =:partner_id AND O.status != 0")->bindValue(":partner_id",Yii::$app->user->identity->partnersid)->queryOne();
        $sp = $sumprice['sumprice'] + ($sumprice['sumextraday']*100);
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sumprice' => $sp,
            'model' => $model,
        ]);
    }

    // ajax
    public function actionGettourorders($id)
    {
        $partner_id = Yii::$app->user->identity->partnersid;
        $dataProvider = new ActiveDataProvider([
            'query' => Orders::find()->where(['tour_id' => $id, 'partner_id' => $partner_id])->orderBy('order_date ASC'),
            'sort' => false,
            //'sort'=> ['defaultOrder' => ['id' => SORT_DESC],'attributes' => []]

        ]);
        return $this->renderPartial('gettourorders', ['dataProvider' => $dataProvider,]);
    }

    public function actionCreateajax()
    {
        $partner_id = Yii::$app->user->identity->partnersid;
        $db = Yii::$app->db;
        $discount = Discounts::findOne(['partner_id'=>$partner_id]);
        $discount = ($discount)?$discount:0;
        $model = new Orders();
        $model->partner_id     =   $partner_id;
        $model->created_date    =   date("Y-m-d H:i:s");

        $modeltour = new Tour();

        if ($modeltour->load(Yii::$app->request->post()) && $modeltour->validate(['guest'])) {
            if ($model->sendEmail()){
                Yii::$app->session->setFlash('success', 'Sifarişlər uğurla əlavə olundu.');

                $orders_sum_price = $db->createCommand("SELECT
                                                    SUM(price)
                                                FROM
                                                    cms_orders                                                 
                                                WHERE
                                                    partner_id =:partner_id AND `status`=0 ")->bindValue(":partner_id",$partner_id)->queryScalar();

                $modeltour ->partner_id = $partner_id;
                $modeltour ->status     = 1;
                $modeltour ->guest     = htmlspecialchars($modeltour->guest);
                $modeltour ->price      = $orders_sum_price;
                if ($modeltour ->save(false))
                    Orders::updateAll(['status' => 1,'tour_id'=>$modeltour->id], '`partner_id` =:partner_id AND `status`=0',[':partner_id'=>$partner_id]);

                echo 'success';

            }
        }else if ($model->load(Yii::$app->request->post()))
        {
            $d  = Prices::findOne($model->price_id);
            $current_order_price        = ($d['price_partner'] + $model->extra_day * $d['partner_extra_day_price']);
            $current_order_total_price  = $current_order_price - (($current_order_price *$discount['percent'])/100);
            $model->price = $current_order_total_price;
            $model->ip      =   Yii::$app->getRequest()->getUserIP();
            $model->save();
            echo 'success';

        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Orders::find()->where(['partner_id'=>$partner_id,'status'=>0]),
            ]);

            $inbasket = $db->createCommand("SELECT                                                  
                                                    count(id) 
                                                FROM
                                                    cms_orders                                                       
                                                WHERE
                                                    partner_id =:partner_id AND status = 0 ")->bindValue(":partner_id",$partner_id)->queryScalar();


            $sumprice = $db->createCommand("SELECT
                                                    sum(P.price_partner + (P.partner_extra_day_price * O.extra_day) ) as price_without_discount,
                                                    sum(O.price) as price_with_discount  
                                                FROM
                                                    cms_orders O
                                                        LEFT JOIN
                                                    cms_prices P ON O.price_id = P.id
                                                WHERE
                                                    O.partner_id =:partner_id AND O.status = 0 ")->bindValue(":partner_id",$partner_id)->queryOne();

            //$sp = $sumprice['extra_day_price_summary'];

            return $this->render('create_ajax', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'sumprice'=>$sumprice,
                'inbasket'=>$inbasket,
                'discount'=>$discount,
                'modeltour'=>$modeltour
            ]);
        }


    }



    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        }
    }
    // ajax
    public function actionValidate() {

        $model = new Orders();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model); Yii::$app->end();

        }


    }
    // ajax
    public function actionPricebyclass($id)
    {
        $partner_id = Yii::$app->user->identity->partnersid;
        $res = Yii::$app->db->createCommand('
                                            SELECT 
                                                P.id, S.title, P.price_partner,P.partner_extra_day_price
                                            FROM
                                                cms_prices P
                                                    LEFT JOIN
                                                cms_services S ON P.service_id = S.id
                                            WHERE
                                                  P.car_type_id = :car_type_id AND S.status = 1 AND P.partner_id = '.$partner_id .';
                                             ')->bindValue(':car_type_id',$id)->queryAll();

        return $this->renderPartial('pricebyclass',['res'=>$res]);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
