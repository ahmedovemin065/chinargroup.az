-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2017 at 06:13 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yii2advanced`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_cars`
--

CREATE TABLE IF NOT EXISTS `cms_cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_type_id` int(11) NOT NULL,
  `driver` varchar(45) NOT NULL,
  `car_number` varchar(45) NOT NULL,
  `make` varchar(45) NOT NULL COMMENT 'marka istehsalci',
  `model` varchar(45) NOT NULL COMMENT 'model',
  `color` varchar(45) NOT NULL,
  `year` smallint(4) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive',
  PRIMARY KEY (`id`),
  KEY `fk_cms_car_cms_car_type1_idx` (`car_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_car_types`
--

CREATE TABLE IF NOT EXISTS `cms_car_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_car_types`
--

INSERT INTO `cms_car_types` (`id`, `title`, `status`) VALUES
(1, 'Vip Aftobus', '1'),
(2, 'Sedan ', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_discounts`
--

CREATE TABLE IF NOT EXISTS `cms_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partners_id` int(11) NOT NULL,
  `percent` double(3,2) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_discounts_cms_partners1_idx` (`partners_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_driver_know_lang`
--

CREATE TABLE IF NOT EXISTS `cms_driver_know_lang` (
  `id` int(11) NOT NULL,
  `langs_id` int(11) NOT NULL,
  `cars_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_driver_know_lang_cms_langs1_idx` (`langs_id`),
  KEY `fk_cms_driver_know_lang_cms_cars1_idx` (`cars_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_langs`
--

CREATE TABLE IF NOT EXISTS `cms_langs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(45) NOT NULL,
  `code` varchar(3) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_orders`
--

CREATE TABLE IF NOT EXISTS `cms_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partners_id` int(11) NOT NULL,
  `prices_id` int(11) NOT NULL,
  `car_id` int(11) DEFAULT NULL,
  `order_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `approve_date` datetime NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0' COMMENT '0- qebul olunub default\n1- icra olunur\n2- bitmish odenilmish',
  PRIMARY KEY (`id`),
  KEY `fk_cms_orders_cms_partners1_idx` (`partners_id`),
  KEY `fk_cms_orders_cms_prices1_idx` (`prices_id`),
  KEY `fk_cms_orders_cms_car1_idx` (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_partners`
--

CREATE TABLE IF NOT EXISTS `cms_partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `contract_date` date NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_partners`
--

INSERT INTO `cms_partners` (`id`, `company`, `email`, `phone`, `address`, `contract_date`, `status`) VALUES
(1, 'test', 'test', '31232313', 'asdasdasd', '2017-11-07', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_payments`
--

CREATE TABLE IF NOT EXISTS `cms_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `discounts_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `price` double(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_payments_cms_orders1_idx` (`orders_id`),
  KEY `fk_cms_payments_cms_discounts1_idx` (`discounts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_prices`
--

CREATE TABLE IF NOT EXISTS `cms_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car_type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `price_partner` double(5,2) NOT NULL,
  `price_driver` double(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cms_prices_cms_car_type_idx` (`car_type_id`),
  KEY `fk_cms_prices_cms_direction1_idx` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_prices`
--

INSERT INTO `cms_prices` (`id`, `car_type_id`, `service_id`, `price_partner`, `price_driver`) VALUES
(1, 1, 1, 20.00, 15.00),
(2, 2, 1, 50.00, 35.00);

-- --------------------------------------------------------

--
-- Table structure for table `cms_services`
--

CREATE TABLE IF NOT EXISTS `cms_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_services`
--

INSERT INTO `cms_services` (`id`, `title`, `status`) VALUES
(1, 'Baki-Quba', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_user`
--

CREATE TABLE IF NOT EXISTS `cms_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partners_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`),
  KEY `fk_cms_user_cms_partners1_idx` (`partners_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_user`
--

INSERT INTO `cms_user` (`id`, `partners_id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'emin', '-kRaUEtw7zOAlS0ljh4rb4EFc04A1_l0', '$2y$13$ueyItqI8wqphIvwC29CWFu7vjZ9yyLuBM84u6cESzmXdETqvAj/2i', NULL, 'ahmedovemin065@gmail.com', 10, 1510310720, 1510310720),
(2, 1, 'testtravel', 'X0XR6L2gr7ZIw5ukRk8uiMNGg2KkP3q5', '$2y$13$KZxTaD.8ECfqMfMXrG6QbesJzErCgCgY3VVBBjK1cJMSJREAuyQXK', NULL, 'testtravel@testtravel.az', 10, 1510430744, 1510430744);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1510310376),
('m130524_201442_init', 1510310386);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cms_cars`
--
ALTER TABLE `cms_cars`
  ADD CONSTRAINT `fk_cms_car_cms_car_type1` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  ADD CONSTRAINT `fk_cms_discounts_cms_partners1` FOREIGN KEY (`partners_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_driver_know_lang`
--
ALTER TABLE `cms_driver_know_lang`
  ADD CONSTRAINT `fk_cms_driver_know_lang_cms_cars1` FOREIGN KEY (`cars_id`) REFERENCES `cms_cars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_driver_know_lang_cms_langs1` FOREIGN KEY (`langs_id`) REFERENCES `cms_langs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_orders`
--
ALTER TABLE `cms_orders`
  ADD CONSTRAINT `fk_cms_orders_cms_car1` FOREIGN KEY (`car_id`) REFERENCES `cms_cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_partners1` FOREIGN KEY (`partners_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_prices1` FOREIGN KEY (`prices_id`) REFERENCES `cms_prices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_payments`
--
ALTER TABLE `cms_payments`
  ADD CONSTRAINT `fk_cms_payments_cms_discounts1` FOREIGN KEY (`discounts_id`) REFERENCES `cms_discounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_payments_cms_orders1` FOREIGN KEY (`orders_id`) REFERENCES `cms_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_prices`
--
ALTER TABLE `cms_prices`
  ADD CONSTRAINT `fk_cms_prices_cms_car_type` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_prices_cms_direction1` FOREIGN KEY (`service_id`) REFERENCES `cms_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_user`
--
ALTER TABLE `cms_user`
  ADD CONSTRAINT `fk_cms_user_cms_partners1` FOREIGN KEY (`partners_id`) REFERENCES `cms_partners` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
