-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2018 at 02:58 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chinar`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_cars`
--

CREATE TABLE `cms_cars` (
  `id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `driver` varchar(45) NOT NULL,
  `car_number` varchar(45) NOT NULL,
  `make` varchar(45) NOT NULL COMMENT 'marka istehsalci',
  `model` varchar(45) NOT NULL COMMENT 'model',
  `color` varchar(45) NOT NULL,
  `year` smallint(4) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_cars`
--

INSERT INTO `cms_cars` (`id`, `car_type_id`, `driver`, `car_number`, `make`, `model`, `color`, `year`, `phone`, `status`) VALUES
(1, 1, 'Əlövsət', '90-xc-192', 'Mercedes', 'V', 'Green', 2011, '', b'1'),
(3, 1, 'Zubil', '90-zp-015', 'Mercedes', 'c222', 'red', 1222, '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_car_types`
--

CREATE TABLE `cms_car_types` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_car_types`
--

INSERT INTO `cms_car_types` (`id`, `title`, `status`) VALUES
(1, 'Sedan Standart', b'1'),
(2, 'Minivenlər 5-8 yerlik', b'1'),
(3, 'Mercedess Sprinter 17-20 yerlik', b'1'),
(4, 'SUV 4*4 və buisness class', b'1'),
(5, 'İsuzu 29-35 yerlik', b'1'),
(6, 'VİP1 növ avtomobillər', b'1'),
(7, 'VİP2 növ avtomobillər', b'1'),
(8, 'Mercedess V class 2015-2017', b'1'),
(9, 'Mercedess 403,44-49 yerlik avtobus', b'1'),
(10, 'Mercedess travego 50-55 yerlik avtobuslar', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_discounts`
--

CREATE TABLE `cms_discounts` (
  `id` int(11) NOT NULL,
  `partners_id` int(11) NOT NULL,
  `percent` double(8,2) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_driver_know_lang`
--

CREATE TABLE `cms_driver_know_lang` (
  `id` int(11) NOT NULL,
  `langs_id` int(11) NOT NULL,
  `cars_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_hotels`
--

CREATE TABLE `cms_hotels` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_hotels`
--

INSERT INTO `cms_hotels` (`id`, `title`, `status`) VALUES
(1, 'Sakitgol Hotel', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_langs`
--

CREATE TABLE `cms_langs` (
  `id` int(11) NOT NULL,
  `lang` varchar(45) NOT NULL,
  `code` varchar(3) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_langs`
--

INSERT INTO `cms_langs` (`id`, `lang`, `code`, `status`) VALUES
(1, 'English', 'en', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_migration`
--

CREATE TABLE `cms_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_migration`
--

INSERT INTO `cms_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1510837388),
('m140608_173539_create_user_table', 1510837552),
('m140611_133903_init_rbac', 1510837553),
('m140808_073114_create_auth_item_group_table', 1510837555),
('m140809_072112_insert_superadmin_to_user', 1510844611);

-- --------------------------------------------------------

--
-- Table structure for table `cms_orders`
--

CREATE TABLE `cms_orders` (
  `id` int(11) NOT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `partner_id` int(11) NOT NULL,
  `prices_id` int(11) NOT NULL,
  `hotels_id` int(11) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  `extra_day` tinyint(3) NOT NULL DEFAULT '0',
  `order_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `approve_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0- qebul olunub default1- icra olunur2- bitmish odenilmish',
  `paid` bit(1) NOT NULL DEFAULT b'0',
  `price` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_orders`
--

INSERT INTO `cms_orders` (`id`, `tour_id`, `partner_id`, `prices_id`, `hotels_id`, `car_id`, `extra_day`, `order_date`, `created_date`, `approve_date`, `status`, `paid`, `price`) VALUES
(104, 43, 1, 12, NULL, NULL, 0, '2018-02-14 10:00:00', '2018-02-13 11:06:18', '0000-00-00 00:00:00', 1, b'0', 45.00),
(105, 43, 1, 18, NULL, NULL, 0, '2018-02-15 10:00:00', '2018-02-13 11:06:34', '0000-00-00 00:00:00', 1, b'0', 70.00),
(106, 43, 1, 20, NULL, NULL, 0, '2018-02-16 10:00:00', '2018-02-13 11:06:49', '0000-00-00 00:00:00', 1, b'0', 70.00),
(107, 43, 1, 22, NULL, NULL, 0, '2018-02-17 09:00:00', '2018-02-13 11:07:05', '0000-00-00 00:00:00', 1, b'0', 90.00),
(111, 44, 1, 10, 1, NULL, 0, '2018-02-22 14:05:00', '2018-02-13 14:04:58', '0000-00-00 00:00:00', 1, b'0', 30.00),
(112, 44, 1, 12, NULL, NULL, 0, '2018-02-09 09:45:00', '2018-02-13 14:11:10', '0000-00-00 00:00:00', 1, b'0', 45.00),
(113, NULL, 1, 10, 1, NULL, 0, '2018-02-02 03:15:00', '2018-02-13 14:17:02', '0000-00-00 00:00:00', 0, b'0', 30.00);

-- --------------------------------------------------------

--
-- Table structure for table `cms_partners`
--

CREATE TABLE `cms_partners` (
  `id` int(11) NOT NULL,
  `company` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `contract_date` date NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_partners`
--

INSERT INTO `cms_partners` (`id`, `company`, `email`, `phone`, `address`, `contract_date`, `status`) VALUES
(1, 'Travel 1', 'travel@travel.az', '31232313', 'asdasdasd', '2017-11-07', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_payments`
--

CREATE TABLE `cms_payments` (
  `id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `discounts_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `price` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_prices`
--

CREATE TABLE `cms_prices` (
  `id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `partners_id` int(11) NOT NULL,
  `price_partner` double(5,2) NOT NULL,
  `price_driver` double(5,2) NOT NULL,
  `partner_extra_day_price` double(5,2) NOT NULL,
  `driver_extra_day_price` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_prices`
--

INSERT INTO `cms_prices` (`id`, `car_type_id`, `service_id`, `partners_id`, `price_partner`, `price_driver`, `partner_extra_day_price`, `driver_extra_day_price`) VALUES
(10, 1, 2, 1, 30.00, 20.00, 0.00, 0.00),
(12, 1, 3, 1, 45.00, 35.00, 0.00, 0.00),
(14, 1, 4, 1, 60.00, 50.00, 0.00, 0.00),
(16, 1, 5, 1, 60.00, 50.00, 0.00, 0.00),
(18, 1, 6, 1, 70.00, 60.00, 0.00, 0.00),
(20, 1, 7, 1, 70.00, 50.00, 0.00, 0.00),
(22, 1, 8, 1, 90.00, 70.00, 0.00, 0.00),
(24, 4, 2, 1, 10.00, 5.00, 10.00, 7.00);

-- --------------------------------------------------------

--
-- Table structure for table `cms_services`
--

CREATE TABLE `cms_services` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_services`
--

INSERT INTO `cms_services` (`id`, `title`, `status`) VALUES
(1, 'Transfer (Havalimanı-Hotel)', b'1'),
(2, 'Bakı turu (Şəhərdaxili) saat 10:00 dan-15:00', b'1'),
(3, 'Bakı turu (Şəhərdaxili) saat 10:00 dan-18:00', b'1'),
(4, 'Bakı turu (Şəhərdaxili) saat 10:00 dan-21:00', b'1'),
(5, 'Abşeron turu (Yanardağ,Atəşgah)', b'1'),
(6, 'Abşeron turu (Yanardağ,Atəşgah)+Şəhər turu', b'1'),
(7, 'Bakı-Qobustan-Bakı', b'1'),
(8, 'Bakı-Şamaxı-Bakı', b'1'),
(9, 'Bakı-Pirqulu-Bakı', b'1'),
(10, 'Bakı-İsmayıllı-Bakı', b'1'),
(11, 'Bakı-Lahıc-Bakı', b'1'),
(12, 'Bakı-Qəbələ-Bakı', b'1'),
(13, 'Bakı-Şəki-Bakı', b'1'),
(14, 'Bakı-Qax-Bakı', b'1'),
(15, 'Bakı-Balakən-Bakı (sərhəd)', b'1'),
(16, 'Bakı-Quba-Bakı', b'1'),
(17, 'Bakı-Qusar-Bakı', b'1'),
(18, 'Bakı-Nabran-Bakı', b'1'),
(19, 'Bakı-Masallı-Bakı', b'1'),
(20, 'Bakı-Lerik –Bakı', b'1'),
(21, 'Bakı-Şahdağ-Bakı', b'1'),
(22, 'Bakı-Gəncə-Bakı', b'1'),
(23, 'Bakı-Naftalan-Bakı', b'1'),
(24, 'Rayonda əlavə gün', b'1'),
(25, 'Rayondan rayona transferlər(qonşu rayonlar)', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_standard_prices`
--

CREATE TABLE `cms_standard_prices` (
  `id` int(11) NOT NULL,
  `car_types_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL,
  `price_partner` double(5,2) NOT NULL,
  `price_driver` double(5,2) NOT NULL,
  `partner_extra_day_price` double(5,2) NOT NULL,
  `driver_extra_day_price` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_standard_prices`
--

INSERT INTO `cms_standard_prices` (`id`, `car_types_id`, `services_id`, `price_partner`, `price_driver`, `partner_extra_day_price`, `driver_extra_day_price`) VALUES
(5, 1, 1, 15.00, 12.00, 80.00, 0.00),
(6, 1, 2, 30.00, 20.00, 0.00, 0.00),
(7, 1, 3, 45.00, 35.00, 0.00, 0.00),
(8, 1, 4, 60.00, 50.00, 0.00, 0.00),
(9, 1, 5, 60.00, 50.00, 0.00, 0.00),
(10, 1, 6, 70.00, 60.00, 0.00, 0.00),
(11, 1, 7, 70.00, 50.00, 0.00, 0.00),
(12, 1, 8, 90.00, 70.00, 80.00, 0.00),
(13, 4, 2, 10.00, 5.00, 10.00, 7.00);

-- --------------------------------------------------------

--
-- Table structure for table `cms_tour`
--

CREATE TABLE `cms_tour` (
  `id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `price` double(5,2) NOT NULL,
  `guest` varchar(45) NOT NULL,
  `paid` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_tour`
--

INSERT INTO `cms_tour` (`id`, `partner_id`, `status`, `create_date`, `price`, `guest`, `paid`) VALUES
(42, 1, 1, '2018-02-13 08:15:06', 10.00, 'saleh', b'0'),
(43, 1, 1, '2018-02-13 11:07:39', 305.00, 'Al Maqdum Al Shirvani', b'0'),
(44, 1, 1, '2018-02-13 13:12:19', 90.00, 'zamiq', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `cms_user`
--

CREATE TABLE `cms_user` (
  `id` int(11) NOT NULL,
  `partners_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cms_user`
--

INSERT INTO `cms_user` (`id`, `partners_id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'emin', '-kRaUEtw7zOAlS0ljh4rb4EFc04A1_l0', '$2y$13$ueyItqI8wqphIvwC29CWFu7vjZ9yyLuBM84u6cESzmXdETqvAj/2i', NULL, 'ahmedovemin065@gmail.com', 10, 1510310720, 1510310720),
(2, 1, 'testtravel', 'X0XR6L2gr7ZIw5ukRk8uiMNGg2KkP3q5', '$2y$13$KZxTaD.8ECfqMfMXrG6QbesJzErCgCgY3VVBBjK1cJMSJREAuyQXK', NULL, 'testtravel@testtravel.az', 10, 1510430744, 1511124696),
(3, 1, 'blabla', 'adCfom_vtcs14duc9YRCsldy3x0m0Ngs', '$2y$13$.4/eShP2YWzVwgO2HW7EZOJzcIDTTBPZlVUGfPPmKTYZFLKzKwWHm', NULL, 'asdasd@asdasdasd.az', 10, 1511122831, 1511125159),
(18, NULL, 'admin', 'uNq6YlDomKwEbf2GSgokKzII3BexwaaK', '$2y$13$e8k7XG3lsbITDjjPSvqsH.kyv4u/qG4n.km4RtKZcMeYo0RrlLEWq', NULL, 'e.ehmedov@database.az', 10, 1518503120, 1518503120);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1510310376),
('m130524_201442_init', 1510310386);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_cars`
--
ALTER TABLE `cms_cars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_car_cms_car_type1_idx` (`car_type_id`);

--
-- Indexes for table `cms_car_types`
--
ALTER TABLE `cms_car_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_discounts_cms_partners1_idx` (`partners_id`);

--
-- Indexes for table `cms_driver_know_lang`
--
ALTER TABLE `cms_driver_know_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_driver_know_lang_cms_langs1_idx` (`langs_id`),
  ADD KEY `fk_cms_driver_know_lang_cms_cars1_idx` (`cars_id`);

--
-- Indexes for table `cms_hotels`
--
ALTER TABLE `cms_hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_langs`
--
ALTER TABLE `cms_langs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_migration`
--
ALTER TABLE `cms_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `cms_orders`
--
ALTER TABLE `cms_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_orders_cms_prices1_idx` (`prices_id`),
  ADD KEY `fk_cms_orders_cms_car1_idx` (`car_id`),
  ADD KEY `fk_cms_orders_cms_hotels1_idx` (`hotels_id`),
  ADD KEY `fk_cms_orders_cms_tour1_idx` (`tour_id`),
  ADD KEY `fk_cms_orders_cms_partners1_idx` (`partner_id`);

--
-- Indexes for table `cms_partners`
--
ALTER TABLE `cms_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_payments`
--
ALTER TABLE `cms_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_payments_cms_orders1_idx` (`orders_id`),
  ADD KEY `fk_cms_payments_cms_discounts1_idx` (`discounts_id`);

--
-- Indexes for table `cms_prices`
--
ALTER TABLE `cms_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_prices_cms_car_type_idx` (`car_type_id`),
  ADD KEY `fk_cms_prices_cms_direction1_idx` (`service_id`),
  ADD KEY `fk_cms_prices_cms_partners1_idx` (`partners_id`);

--
-- Indexes for table `cms_services`
--
ALTER TABLE `cms_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_standard_prices`
--
ALTER TABLE `cms_standard_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_standard_prices_cms_car_types1_idx` (`car_types_id`),
  ADD KEY `fk_cms_standard_prices_cms_services1_idx` (`services_id`);

--
-- Indexes for table `cms_tour`
--
ALTER TABLE `cms_tour`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_tour_cms_partners1_idx` (`partner_id`);

--
-- Indexes for table `cms_user`
--
ALTER TABLE `cms_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `fk_cms_user_cms_partners1_idx` (`partners_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_cars`
--
ALTER TABLE `cms_cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cms_car_types`
--
ALTER TABLE `cms_car_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_hotels`
--
ALTER TABLE `cms_hotels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_langs`
--
ALTER TABLE `cms_langs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_orders`
--
ALTER TABLE `cms_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `cms_partners`
--
ALTER TABLE `cms_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_payments`
--
ALTER TABLE `cms_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_prices`
--
ALTER TABLE `cms_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `cms_services`
--
ALTER TABLE `cms_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `cms_standard_prices`
--
ALTER TABLE `cms_standard_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cms_tour`
--
ALTER TABLE `cms_tour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `cms_user`
--
ALTER TABLE `cms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cms_cars`
--
ALTER TABLE `cms_cars`
  ADD CONSTRAINT `fk_cms_car_cms_car_type1` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  ADD CONSTRAINT `fk_cms_discounts_cms_partners1` FOREIGN KEY (`partners_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_driver_know_lang`
--
ALTER TABLE `cms_driver_know_lang`
  ADD CONSTRAINT `fk_cms_driver_know_lang_cms_cars1` FOREIGN KEY (`cars_id`) REFERENCES `cms_cars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_driver_know_lang_cms_langs1` FOREIGN KEY (`langs_id`) REFERENCES `cms_langs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_orders`
--
ALTER TABLE `cms_orders`
  ADD CONSTRAINT `fk_cms_orders_cms_car1` FOREIGN KEY (`car_id`) REFERENCES `cms_cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_hotels1` FOREIGN KEY (`hotels_id`) REFERENCES `cms_hotels` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_cms_orders_cms_partners1` FOREIGN KEY (`partner_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_prices1` FOREIGN KEY (`prices_id`) REFERENCES `cms_prices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_tour1` FOREIGN KEY (`tour_id`) REFERENCES `cms_tour` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_payments`
--
ALTER TABLE `cms_payments`
  ADD CONSTRAINT `fk_cms_payments_cms_discounts1` FOREIGN KEY (`discounts_id`) REFERENCES `cms_discounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_payments_cms_orders1` FOREIGN KEY (`orders_id`) REFERENCES `cms_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_prices`
--
ALTER TABLE `cms_prices`
  ADD CONSTRAINT `fk_cms_prices_cms_car_type` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_prices_cms_direction1` FOREIGN KEY (`service_id`) REFERENCES `cms_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_prices_cms_partners1` FOREIGN KEY (`partners_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_standard_prices`
--
ALTER TABLE `cms_standard_prices`
  ADD CONSTRAINT `fk_cms_standard_prices_cms_car_types1` FOREIGN KEY (`car_types_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_standard_prices_cms_services1` FOREIGN KEY (`services_id`) REFERENCES `cms_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_tour`
--
ALTER TABLE `cms_tour`
  ADD CONSTRAINT `fk_cms_tour_cms_partners1` FOREIGN KEY (`partner_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_user`
--
ALTER TABLE `cms_user`
  ADD CONSTRAINT `fk_cms_user_cms_partners1` FOREIGN KEY (`partners_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
