-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2018 at 02:34 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chinar`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_cars`
--

CREATE TABLE `cms_cars` (
  `id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `driver` varchar(45) NOT NULL,
  `car_number` varchar(45) NOT NULL,
  `make` varchar(45) NOT NULL COMMENT 'marka istehsalci',
  `model` varchar(45) NOT NULL COMMENT 'model',
  `color` varchar(45) NOT NULL,
  `year` smallint(4) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive',
  `driver_gets` double(5,2) NOT NULL,
  `last_payment_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_cars`
--

INSERT INTO `cms_cars` (`id`, `car_type_id`, `driver`, `car_number`, `make`, `model`, `color`, `year`, `phone`, `status`, `driver_gets`, `last_payment_date`) VALUES
(4, 2, 'Reşad Behbudov', '99-XH-979', 'Mercedess', 'Vito', 'Qara', 2014, '+994557333388', b'1', 0.00, '0000-00-00 00:00:00'),
(5, 2, 'Pervin İsmayilov', '99-KK-505', 'Mercedess', 'Vito', 'Qara', 2010, '+994554118844', b'1', 0.00, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_car_types`
--

CREATE TABLE `cms_car_types` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_car_types`
--

INSERT INTO `cms_car_types` (`id`, `title`, `status`) VALUES
(1, 'Sedan Standart', b'1'),
(2, 'Minivenlər 5-8 yerlik', b'1'),
(3, 'Mercedess Sprinter 17-20 yerlik', b'1'),
(4, 'SUV 4*4 və buisness class', b'1'),
(5, 'İsuzu 29-35 yerlik', b'1'),
(6, 'VİP1 növ avtomobillər', b'1'),
(7, 'VİP2 növ avtomobillər', b'1'),
(8, 'Mercedess V class 2015-2017', b'1'),
(9, 'Mercedess 403,44-49 yerlik avtobus', b'1'),
(10, 'Mercedess travego 50-55 yerlik avtobuslar', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_discounts`
--

CREATE TABLE `cms_discounts` (
  `id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `percent` double(8,2) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_driver_know_lang`
--

CREATE TABLE `cms_driver_know_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_hotels`
--

CREATE TABLE `cms_hotels` (
  `id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_hotels`
--

INSERT INTO `cms_hotels` (`id`, `title`, `status`) VALUES
(1, 'Sakitgol Hotel', b'1'),
(2, 'Boulvared Hotel', b'1'),
(3, 'Intourist Hotel', b'1'),
(4, 'Hilton Hotel', b'1'),
(5, 'Anatolia Hotel', b'1'),
(6, 'Miraj İnn Boutique Hotel', b'1'),
(7, 'Qorqud Hotel', b'1'),
(8, 'Arriva Hotel', b'1'),
(9, 'IVA Hotel', b'1'),
(10, 'Red Roof Hotel', b'1'),
(11, 'Ramada Baku', b'1'),
(12, 'Four Seasons Hotel Baku', b'1'),
(13, 'Smith Hotel', b'1'),
(14, 'Austin Hotel Baku', b'1'),
(15, 'Ramada Hotel &amp; Suites Baku', b'1'),
(16, 'Jasmine Hotel', b'1'),
(17, 'Pullman Baku', b'1'),
(18, 'Fairmont Baku', b'1'),
(19, 'Flame Towers', b'1'),
(20, 'Boyuk Gala Hotel', b'1'),
(21, 'Hale Kai Hotel', b'1'),
(22, 'Excelsior Hotel &amp; Spa Baku', b'1'),
(23, 'Baku Palace Hotel', b'1'),
(24, 'Diplomat Hotel', b'1'),
(25, 'Staybridge Suites Baku', b'1'),
(26, 'Central Park Hotel', b'1'),
(27, 'Old East Hotel', b'1'),
(28, 'Gəncəli Plaza Baku', b'1'),
(29, 'Holiday İnn Baku', b'1'),
(30, 'AYF Palace', b'1'),
(31, 'Riviera Hotel', b'1'),
(32, 'Qafqaz Park Hotel', b'1'),
(33, 'Sun Rise Hotel', b'1'),
(34, 'Karat İnn Hotel', b'1'),
(35, 'Passage Boutique Hotel', b'1'),
(36, 'Du Port Hotel', b'1'),
(37, 'Astoria Baku Hotel', b'1'),
(38, 'BP Hotel Baku', b'1'),
(39, 'İron Hotel', b'1'),
(40, 'Hotel İrshad', b'1'),
(41, 'AEF Hotel', b'1'),
(42, 'JW Marriott Absheron Hotel', b'1'),
(43, 'Hyatt Regency Baku', b'1'),
(44, 'Europe Hotel', b'1'),
(45, 'Hilton Hotel Baku', b'1'),
(46, 'Park İnn Hotel', b'1'),
(47, 'Premier Hotel', b'1'),
(48, 'New Baku Hotel', b'1'),
(49, 'Qafqaz Hotel', b'1'),
(50, 'Landmark Hotel', b'1'),
(51, 'Qafqaz Point Boutique Hotel', b'1'),
(52, 'Ariva Hotel', b'1'),
(53, 'Avand Hotel', b'1'),
(54, 'Safran Hotel', b'1'),
(55, 'İntourist Hotel Baku', b'1'),
(56, 'Amber Hotel', b'1'),
(57, 'Sapphire City Hotel', b'1'),
(58, 'Shah Palace Hotel', b'1'),
(59, 'Caspian Palace Hotel', b'1'),
(60, 'Teatro Boutique Hotel', b'1'),
(61, 'Chiraq Plaza Hotel', b'1'),
(62, 'Bosfor Hotel Baku', b'1'),
(63, 'Planet İnn Hotel Baku', b'1'),
(64, 'Days Hotel Baku', b'1'),
(65, 'Boulevard Hotel Baku', b'1'),
(66, 'Intourist Hotel', b'1'),
(67, 'Consul Hotel', b'1'),
(68, 'Sapphire İnn Hotel', b'1'),
(69, 'Nemi Hotel', b'1'),
(70, 'Royal Hotel', b'1'),
(71, 'Briston Hotel', b'1'),
(72, 'Divan Express Hotel', b'1'),
(73, 'Egoist Hotel', b'1'),
(74, 'Winter Park Hotel', b'1'),
(75, 'Jireh Hotel', b'1'),
(76, 'Azcot Hotel', b'1'),
(77, 'Old Baku Hotel', b'1'),
(78, 'Lake Palace', b'1'),
(79, 'Askar Hotel', b'1'),
(80, 'Old Gates Hotel', b'1'),
(81, 'Renaissance Palace Baku', b'1'),
(82, 'Empire Hotel', b'1'),
(83, 'Boutique Hotel Baku', b'1'),
(84, 'Riva Hotel Baku', b'1'),
(85, 'Atfk Hotel', b'1'),
(86, 'Travel Hotel', b'1'),
(87, 'Sultan İnn Boutique Hotel', b'1'),
(88, 'East Legent Panorama Hotel', b'1'),
(89, 'Boulevard Side Hotel', b'1'),
(90, 'Rich Hotel Baku', b'1'),
(91, 'Paradise Hotel Baku', b'1'),
(92, 'Horizon Hotel', b'1'),
(93, 'Vania Hotel', b'1'),
(94, 'The Crown Hotel', b'1'),
(95, 'Swan Hotel Baku', b'1'),
(96, 'Old street Boutique', b'1'),
(97, 'Grand Hotel', b'1'),
(98, 'Caspian Hotel', b'1'),
(99, 'Seven Rooms Hotel', b'1'),
(100, 'Emerald Hotel Baku', b'1'),
(101, 'La Villa Hotel', b'1'),
(102, 'Boutique Palace', b'1'),
(103, 'Golden Coast', b'1'),
(104, 'Don-Dar Hotel', b'1'),
(105, 'Caspian Business Hotel', b'1'),
(106, 'Balion Hotel', b'1'),
(107, 'İva Hotel', b'1'),
(108, 'Terrace Hotel', b'1'),
(109, 'Museum İnn', b'1'),
(110, 'Garden Villa Hotel', b'1'),
(111, 'Clover Hotel', b'1'),
(112, 'Xudaferin Hotel', b'1'),
(113, 'Rigs Hotel', b'1'),
(114, 'Old City İnn', b'1'),
(115, 'All Seasons Hotel', b'1'),
(116, 'Nur-2 Hotel', b'1'),
(117, 'Comfort İnn', b'1'),
(118, 'Garden City Hotel', b'1'),
(119, 'Xan Çinar Hotel', b'1'),
(120, 'İstabul Hotel', b'1'),
(121, 'Teymur Mini Hotel', b'1'),
(122, '12 İnn Bulvar', b'1'),
(123, 'Avesta Hotel', b'1'),
(124, 'Black Mount Hotel', b'1'),
(125, 'City Walls Hotel', b'1'),
(126, 'Homebridge Hotel', b'1'),
(127, 'Galery Hotel Baku', b'1'),
(128, 'History Boutique Hotel', b'1'),
(129, 'Abu Turan Hotel', b'1'),
(130, 'Hotel Sea Port Hotel', b'1'),
(131, 'Radisson Sas Plaza Hotel', b'1'),
(132, 'Deniz İnn Boutique Hotel', b'1'),
(133, 'Rumya Hotel', b'1'),
(134, 'Titanik Hotel', b'1'),
(135, 'Avenue Hotel', b'1'),
(136, 'Bayil İnn Hotel', b'1'),
(137, 'Viva Boutique Hotel', b'1'),
(138, 'Two Seasons Boutique Hotel', b'1'),
(139, 'Sapphire Marine Hotel', b'1'),
(140, 'Gallery Hotel Baku', b'1'),
(141, 'Atropat Hotel', b'1'),
(142, 'Gallery Lux', b'1'),
(143, 'Serin Hotel', b'1'),
(144, 'Monolit Plaza', b'1'),
(145, 'Continent Hotel', b'1'),
(146, 'Deluxe City Hotel', b'1'),
(147, 'Nord West Hotel', b'1'),
(148, 'Aysberq Resort', b'1'),
(149, 'Nika Hotel', b'1'),
(150, 'Life Holiday', b'1'),
(151, 'Clear Hotel', b'1'),
(152, 'Delfin Hotel', b'1'),
(153, 'Ruma Port Hotel', b'1'),
(154, 'Elite Hotel', b'1'),
(155, 'KBT Boutique Hotel', b'1'),
(156, 'Hotel Baku Comfort Rooms', b'1'),
(157, 'Neapol Hotel', b'1'),
(158, 'Shams Hotel', b'1'),
(159, 'Joyy Hotel', b'1'),
(160, 'Modern Hotel', b'1'),
(161, 'Azalea Hotel Baku', b'1'),
(162, 'City Palace Hotel', b'1'),
(163, 'Rixos Quba Azerbaijan', b'1'),
(164, 'Qafqaz Karvansaray Hotel', b'1'),
(165, 'Qafqaz Sport Hotel', b'1'),
(166, 'QafqaZ Qabala City Hotel', b'1'),
(167, 'Qəbələ şəhər oteli', b'1'),
(168, 'Qafqaz Resort Hotel', b'1'),
(169, 'Qafqaz Tufandag Mountain Resort Hotel', b'1'),
(170, 'Qafqaz Riverside Resort Hotel', b'1'),
(171, 'Elelef park hotel', b'1'),
(172, 'Nohur Hotel', b'1'),
(173, 'Gilan Hotel', b'1'),
(174, 'Qafqaz Yeddi Gozel Hotel', b'1'),
(175, 'Terrace Hotel Quba', b'1'),
(176, 'Shane Hotel Quba', b'1'),
(177, 'Rixos Quba Azerbaijan', b'1'),
(178, 'Cənnət Bağı Quba istirahət mərkəzi', b'1'),
(179, 'Oskar Hotel', b'1'),
(180, 'Shahdag Guba Hotel &amp; Spa', b'1'),
(181, 'MestDergah', b'1'),
(182, 'Bərpa SPA Mərkəzi', b'1'),
(183, '&quot;Quba&quot; İstirahət Mərkəzi', b'1'),
(184, 'Aynur Hotel&amp;Restaurant', b'1'),
(185, 'Quba Qechresh Holiday House', b'1'),
(186, 'Quba Retro Hotel', b'1'),
(187, 'Qusar Olimpik Hotel', b'1'),
(188, 'Ladera Resort', b'1'),
(189, 'Zirve Hotel Shahdag', b'1'),
(190, 'Şahdağ Turizm Kompleksi', b'1'),
(191, 'Shahdag Hotel &amp; Spa', b'1'),
(192, 'Butik hotel RA', b'1'),
(193, 'P.I.K. Palace', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_langs`
--

CREATE TABLE `cms_langs` (
  `id` int(11) NOT NULL,
  `lang` varchar(45) NOT NULL,
  `code` varchar(3) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_langs`
--

INSERT INTO `cms_langs` (`id`, `lang`, `code`, `status`) VALUES
(1, 'English', 'en', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_orders`
--

CREATE TABLE `cms_orders` (
  `id` int(11) NOT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `partner_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  `extra_day` tinyint(3) NOT NULL DEFAULT '0',
  `order_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `approve_date` datetime NOT NULL,
  `pay_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0- qebul olunub default1- icra olunur2- bitmish odenilmish',
  `paid` bit(1) NOT NULL DEFAULT b'0',
  `price` double(5,2) NOT NULL,
  `ip` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_orders`
--

INSERT INTO `cms_orders` (`id`, `tour_id`, `partner_id`, `price_id`, `hotel_id`, `car_id`, `extra_day`, `order_date`, `created_date`, `approve_date`, `pay_date`, `status`, `paid`, `price`, `ip`) VALUES
(139, 12, 2, 63, 1, NULL, 0, '2018-02-21 06:30:00', '2018-02-19 02:13:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, b'0', 15.00, '::1'),
(140, 13, 2, 64, 2, NULL, 0, '2018-02-21 10:30:00', '2018-02-19 02:16:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, b'0', 30.00, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_partners`
--

CREATE TABLE `cms_partners` (
  `id` int(11) NOT NULL,
  `company` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `contract_date` date NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_partners`
--

INSERT INTO `cms_partners` (`id`, `company`, `email`, `phone`, `address`, `contract_date`, `status`) VALUES
(2, 'RealTravel', 'info@realt.com', '+(99455) 632-21-26', 'Baki 1155', '2018-02-19', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_payments`
--

CREATE TABLE `cms_payments` (
  `id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `price` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_prices`
--

CREATE TABLE `cms_prices` (
  `id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `price_partner` double(5,2) NOT NULL,
  `price_driver` double(5,2) NOT NULL,
  `partner_extra_day_price` double(5,2) NOT NULL,
  `driver_extra_day_price` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_prices`
--

INSERT INTO `cms_prices` (`id`, `car_type_id`, `service_id`, `partner_id`, `price_partner`, `price_driver`, `partner_extra_day_price`, `driver_extra_day_price`) VALUES
(63, 1, 1, 2, 15.00, 12.00, 80.00, 70.00),
(64, 1, 2, 2, 30.00, 20.00, 0.00, 0.00),
(65, 1, 3, 2, 45.00, 35.00, 0.00, 0.00),
(66, 1, 4, 2, 60.00, 50.00, 0.00, 0.00),
(67, 1, 5, 2, 60.00, 50.00, 0.00, 0.00),
(68, 1, 6, 2, 70.00, 60.00, 0.00, 0.00),
(69, 1, 7, 2, 70.00, 50.00, 0.00, 0.00),
(70, 1, 8, 2, 90.00, 70.00, 80.00, 0.00),
(71, 1, 9, 2, 100.00, 80.00, 80.00, 60.00),
(72, 1, 10, 2, 110.00, 90.00, 80.00, 60.00),
(73, 1, 11, 2, 120.00, 100.00, 80.00, 60.00),
(74, 1, 12, 2, 130.00, 120.00, 80.00, 60.00),
(75, 1, 13, 2, 160.00, 140.00, 80.00, 60.00),
(76, 1, 14, 2, 170.00, 160.00, 80.00, 60.00),
(77, 1, 15, 2, 180.00, 160.00, 80.00, 60.00),
(78, 1, 16, 2, 110.00, 90.00, 80.00, 60.00),
(79, 1, 17, 2, 120.00, 120.00, 80.00, 60.00),
(80, 1, 18, 2, 130.00, 110.00, 80.00, 60.00),
(81, 1, 19, 2, 130.00, 110.00, 80.00, 60.00),
(82, 1, 20, 2, 170.00, 140.00, 80.00, 60.00),
(83, 1, 21, 2, 130.00, 120.00, 80.00, 60.00),
(84, 1, 22, 2, 190.00, 160.00, 80.00, 60.00),
(85, 1, 23, 2, 150.00, 140.00, 80.00, 60.00),
(86, 2, 1, 2, 30.00, 25.00, 0.00, 0.00),
(87, 2, 2, 2, 50.00, 40.00, 0.00, 0.00),
(88, 2, 3, 2, 60.00, 50.00, 0.00, 0.00),
(89, 2, 4, 2, 70.00, 60.00, 0.00, 0.00),
(90, 2, 5, 2, 70.00, 60.00, 0.00, 0.00),
(91, 2, 6, 2, 80.00, 70.00, 0.00, 0.00),
(92, 2, 7, 2, 90.00, 70.00, 0.00, 0.00),
(93, 2, 8, 2, 110.00, 90.00, 80.00, 60.00),
(94, 2, 9, 2, 120.00, 100.00, 80.00, 60.00),
(95, 2, 10, 2, 135.00, 110.00, 80.00, 60.00),
(96, 2, 11, 2, 140.00, 120.00, 80.00, 60.00),
(97, 2, 12, 2, 160.00, 150.00, 80.00, 60.00),
(98, 2, 13, 2, 200.00, 180.00, 80.00, 60.00),
(99, 2, 14, 2, 240.00, 200.00, 80.00, 60.00),
(100, 2, 15, 2, 250.00, 220.00, 80.00, 60.00),
(101, 2, 16, 2, 140.00, 120.00, 80.00, 60.00),
(102, 2, 17, 2, 160.00, 140.00, 80.00, 60.00),
(103, 2, 18, 2, 160.00, 150.00, 80.00, 60.00),
(104, 2, 19, 2, 150.00, 130.00, 80.00, 60.00),
(105, 2, 20, 2, 190.00, 170.00, 80.00, 60.00),
(106, 2, 21, 2, 160.00, 150.00, 80.00, 60.00),
(107, 2, 22, 2, 220.00, 150.00, 80.00, 60.00),
(108, 2, 23, 2, 200.00, 150.00, 80.00, 60.00);

-- --------------------------------------------------------

--
-- Table structure for table `cms_services`
--

CREATE TABLE `cms_services` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_services`
--

INSERT INTO `cms_services` (`id`, `title`, `status`) VALUES
(1, 'Transfer (Havalimanı-Hotel)', b'1'),
(2, 'Bakı turu (Şəhərdaxili) saat 10:00 dan-15:00', b'1'),
(3, 'Bakı turu (Şəhərdaxili) saat 10:00 dan-18:00', b'1'),
(4, 'Bakı turu (Şəhərdaxili) saat 10:00 dan-21:00', b'1'),
(5, 'Abşeron turu (Yanardağ,Atəşgah)', b'1'),
(6, 'Abşeron turu (Yanardağ,Atəşgah)+Şəhər turu', b'1'),
(7, 'Bakı-Qobustan-Bakı', b'1'),
(8, 'Bakı-Şamaxı-Bakı', b'1'),
(9, 'Bakı-Pirqulu-Bakı', b'1'),
(10, 'Bakı-İsmayıllı-Bakı', b'1'),
(11, 'Bakı-Lahıc-Bakı', b'1'),
(12, 'Bakı-Qəbələ-Bakı', b'1'),
(13, 'Bakı-Şəki-Bakı', b'1'),
(14, 'Bakı-Qax-Bakı', b'1'),
(15, 'Bakı-Balakən-Bakı (sərhəd)', b'1'),
(16, 'Bakı-Quba-Bakı', b'1'),
(17, 'Bakı-Qusar-Bakı', b'1'),
(18, 'Bakı-Nabran-Bakı', b'1'),
(19, 'Bakı-Masallı-Bakı', b'1'),
(20, 'Bakı-Lerik –Bakı', b'1'),
(21, 'Bakı-Şahdağ-Bakı', b'1'),
(22, 'Bakı-Gəncə-Bakı', b'1'),
(23, 'Bakı-Naftalan-Bakı', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_standard_prices`
--

CREATE TABLE `cms_standard_prices` (
  `id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `price_partner` double(5,2) NOT NULL,
  `price_driver` double(5,2) NOT NULL,
  `partner_extra_day_price` double(5,2) NOT NULL,
  `driver_extra_day_price` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_standard_prices`
--

INSERT INTO `cms_standard_prices` (`id`, `car_type_id`, `service_id`, `price_partner`, `price_driver`, `partner_extra_day_price`, `driver_extra_day_price`) VALUES
(5, 1, 1, 15.00, 12.00, 80.00, 70.00),
(6, 1, 2, 30.00, 20.00, 0.00, 0.00),
(7, 1, 3, 45.00, 35.00, 0.00, 0.00),
(8, 1, 4, 60.00, 50.00, 0.00, 0.00),
(9, 1, 5, 60.00, 50.00, 0.00, 0.00),
(10, 1, 6, 70.00, 60.00, 0.00, 0.00),
(11, 1, 7, 70.00, 50.00, 0.00, 0.00),
(12, 1, 8, 90.00, 70.00, 80.00, 0.00),
(14, 1, 9, 100.00, 80.00, 80.00, 60.00),
(15, 1, 10, 110.00, 90.00, 80.00, 60.00),
(16, 1, 11, 120.00, 100.00, 80.00, 60.00),
(17, 1, 12, 130.00, 120.00, 80.00, 60.00),
(18, 1, 13, 160.00, 140.00, 80.00, 60.00),
(19, 1, 14, 170.00, 160.00, 80.00, 60.00),
(20, 1, 15, 180.00, 160.00, 80.00, 60.00),
(21, 1, 16, 110.00, 90.00, 80.00, 60.00),
(22, 1, 17, 120.00, 120.00, 80.00, 60.00),
(23, 1, 18, 130.00, 110.00, 80.00, 60.00),
(24, 1, 19, 130.00, 110.00, 80.00, 60.00),
(25, 1, 20, 170.00, 140.00, 80.00, 60.00),
(26, 1, 21, 130.00, 120.00, 80.00, 60.00),
(27, 1, 22, 190.00, 160.00, 80.00, 60.00),
(28, 1, 23, 150.00, 140.00, 80.00, 60.00),
(29, 2, 1, 30.00, 25.00, 0.00, 0.00),
(30, 2, 2, 50.00, 40.00, 0.00, 0.00),
(31, 2, 3, 60.00, 50.00, 0.00, 0.00),
(32, 2, 4, 70.00, 60.00, 0.00, 0.00),
(33, 2, 5, 70.00, 60.00, 0.00, 0.00),
(34, 2, 6, 80.00, 70.00, 0.00, 0.00),
(35, 2, 7, 90.00, 70.00, 0.00, 0.00),
(36, 2, 8, 110.00, 90.00, 80.00, 60.00),
(37, 2, 9, 120.00, 100.00, 80.00, 60.00),
(38, 2, 10, 135.00, 110.00, 80.00, 60.00),
(39, 2, 11, 140.00, 120.00, 80.00, 60.00),
(40, 2, 12, 160.00, 150.00, 80.00, 60.00),
(41, 2, 13, 200.00, 180.00, 80.00, 60.00),
(42, 2, 14, 240.00, 200.00, 80.00, 60.00),
(43, 2, 15, 250.00, 220.00, 80.00, 60.00),
(44, 2, 16, 140.00, 120.00, 80.00, 60.00),
(45, 2, 17, 160.00, 140.00, 80.00, 60.00),
(46, 2, 18, 160.00, 150.00, 80.00, 60.00),
(47, 2, 19, 150.00, 130.00, 80.00, 60.00),
(48, 2, 20, 190.00, 170.00, 80.00, 60.00),
(49, 2, 21, 160.00, 150.00, 80.00, 60.00),
(50, 2, 22, 220.00, 150.00, 80.00, 60.00),
(51, 2, 23, 200.00, 150.00, 80.00, 60.00);

-- --------------------------------------------------------

--
-- Table structure for table `cms_tours`
--

CREATE TABLE `cms_tours` (
  `id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `price` double(5,2) NOT NULL,
  `guest` varchar(45) NOT NULL,
  `paid` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_tours`
--

INSERT INTO `cms_tours` (`id`, `partner_id`, `status`, `create_date`, `price`, `guest`, `paid`) VALUES
(12, 2, 1, '2018-02-18 22:14:16', 15.00, 'Ramin', b'0'),
(13, 2, 1, '2018-02-18 22:16:36', 30.00, 'Ramiz', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `cms_user`
--

CREATE TABLE `cms_user` (
  `id` int(11) NOT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cms_user`
--

INSERT INTO `cms_user` (`id`, `partner_id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'emin', '-kRaUEtw7zOAlS0ljh4rb4EFc04A1_l0', '$2y$13$ueyItqI8wqphIvwC29CWFu7vjZ9yyLuBM84u6cESzmXdETqvAj/2i', NULL, 'ahmedovemin0651@gmail.com', 10, 1510310720, 1518558666),
(18, NULL, 'admin', 'uNq6YlDomKwEbf2GSgokKzII3BexwaaK', '$2y$13$30/kdAHZwHchOzy1YNqDje07V1XUF7luWo4SeAmPs5RmJJ5XNgzhi', NULL, 'e.ehmedov@database.az', 0, 1518503120, 1518991474),
(21, 2, 'real', '12Yc-mtQObVd0eUCTLQyPqa5jzIWiCFd', '$2y$13$kLiHvvJjyvXHz.6mEHHyCuZp0WSVJSsKTnyoRnirnKQXA5v5fpKVK', NULL, 'regeatravelaz@gmail.com', 10, 1518991893, 1518991893);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_cars`
--
ALTER TABLE `cms_cars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_car_cms_car_type1_idx` (`car_type_id`);

--
-- Indexes for table `cms_car_types`
--
ALTER TABLE `cms_car_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_discounts_cms_partners1_idx` (`partner_id`);

--
-- Indexes for table `cms_driver_know_lang`
--
ALTER TABLE `cms_driver_know_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_driver_know_lang_cms_langs1_idx` (`lang_id`),
  ADD KEY `fk_cms_driver_know_lang_cms_cars1_idx` (`car_id`);

--
-- Indexes for table `cms_hotels`
--
ALTER TABLE `cms_hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_langs`
--
ALTER TABLE `cms_langs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_orders`
--
ALTER TABLE `cms_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_orders_cms_prices1_idx` (`price_id`),
  ADD KEY `fk_cms_orders_cms_car1_idx` (`car_id`),
  ADD KEY `fk_cms_orders_cms_hotels1_idx` (`hotel_id`),
  ADD KEY `fk_cms_orders_cms_tour1_idx` (`tour_id`),
  ADD KEY `fk_cms_orders_cms_partners1_idx` (`partner_id`);

--
-- Indexes for table `cms_partners`
--
ALTER TABLE `cms_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_payments`
--
ALTER TABLE `cms_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_payments_cms_cars1_idx` (`car_id`);

--
-- Indexes for table `cms_prices`
--
ALTER TABLE `cms_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_prices_cms_car_type_idx` (`car_type_id`),
  ADD KEY `fk_cms_prices_cms_direction1_idx` (`service_id`),
  ADD KEY `fk_cms_prices_cms_partners1_idx` (`partner_id`);

--
-- Indexes for table `cms_services`
--
ALTER TABLE `cms_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_standard_prices`
--
ALTER TABLE `cms_standard_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_standard_prices_cms_car_types1_idx` (`car_type_id`),
  ADD KEY `fk_cms_standard_prices_cms_services1_idx` (`service_id`);

--
-- Indexes for table `cms_tours`
--
ALTER TABLE `cms_tours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_tour_cms_partners1_idx` (`partner_id`);

--
-- Indexes for table `cms_user`
--
ALTER TABLE `cms_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `fk_cms_user_cms_partners1_idx` (`partner_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_cars`
--
ALTER TABLE `cms_cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cms_car_types`
--
ALTER TABLE `cms_car_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_hotels`
--
ALTER TABLE `cms_hotels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `cms_langs`
--
ALTER TABLE `cms_langs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_orders`
--
ALTER TABLE `cms_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `cms_partners`
--
ALTER TABLE `cms_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_payments`
--
ALTER TABLE `cms_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cms_prices`
--
ALTER TABLE `cms_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `cms_services`
--
ALTER TABLE `cms_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `cms_standard_prices`
--
ALTER TABLE `cms_standard_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `cms_tours`
--
ALTER TABLE `cms_tours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `cms_user`
--
ALTER TABLE `cms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cms_cars`
--
ALTER TABLE `cms_cars`
  ADD CONSTRAINT `fk_cms_car_cms_car_type1` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  ADD CONSTRAINT `fk_cms_discounts_cms_partners1` FOREIGN KEY (`partner_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_driver_know_lang`
--
ALTER TABLE `cms_driver_know_lang`
  ADD CONSTRAINT `fk_cms_driver_know_lang_cms_cars1` FOREIGN KEY (`car_id`) REFERENCES `cms_cars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_driver_know_lang_cms_langs1` FOREIGN KEY (`lang_id`) REFERENCES `cms_langs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_orders`
--
ALTER TABLE `cms_orders`
  ADD CONSTRAINT `fk_cms_orders_cms_car1` FOREIGN KEY (`car_id`) REFERENCES `cms_cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_hotels1` FOREIGN KEY (`hotel_id`) REFERENCES `cms_hotels` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_cms_orders_cms_partners1` FOREIGN KEY (`partner_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_prices1` FOREIGN KEY (`price_id`) REFERENCES `cms_prices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_tour1` FOREIGN KEY (`tour_id`) REFERENCES `cms_tours` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_payments`
--
ALTER TABLE `cms_payments`
  ADD CONSTRAINT `fk_cms_payments_cms_cars1` FOREIGN KEY (`car_id`) REFERENCES `cms_cars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_prices`
--
ALTER TABLE `cms_prices`
  ADD CONSTRAINT `fk_cms_prices_cms_car_type` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_prices_cms_direction1` FOREIGN KEY (`service_id`) REFERENCES `cms_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_prices_cms_partners1` FOREIGN KEY (`partner_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_standard_prices`
--
ALTER TABLE `cms_standard_prices`
  ADD CONSTRAINT `fk_cms_standard_prices_cms_car_types1` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_standard_prices_cms_services1` FOREIGN KEY (`service_id`) REFERENCES `cms_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_tours`
--
ALTER TABLE `cms_tours`
  ADD CONSTRAINT `fk_cms_tour_cms_partners1` FOREIGN KEY (`partner_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_user`
--
ALTER TABLE `cms_user`
  ADD CONSTRAINT `fk_cms_user_cms_partners1` FOREIGN KEY (`partner_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
