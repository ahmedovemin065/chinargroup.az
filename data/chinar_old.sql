-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2017 at 02:41 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chinar`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_cars`
--

CREATE TABLE `cms_cars` (
  `id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `driver` varchar(45) NOT NULL,
  `car_number` varchar(45) NOT NULL,
  `make` varchar(45) NOT NULL COMMENT 'marka istehsalci',
  `model` varchar(45) NOT NULL COMMENT 'model',
  `color` varchar(45) NOT NULL,
  `year` smallint(4) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_car_types`
--

CREATE TABLE `cms_car_types` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_car_types`
--

INSERT INTO `cms_car_types` (`id`, `title`, `status`) VALUES
(1, 'Vip Aftobus', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_discounts`
--

CREATE TABLE `cms_discounts` (
  `id` int(11) NOT NULL,
  `partners_id` int(11) NOT NULL,
  `percent` varchar(45) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_orders`
--

CREATE TABLE `cms_orders` (
  `id` int(11) NOT NULL,
  `partners_id` int(11) NOT NULL,
  `prices_id` int(11) NOT NULL,
  `car_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approve_date` datetime NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0' COMMENT '0- qebul olunub default\n1- icra olunur\n2- bitmish odenilmish'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_partners`
--

CREATE TABLE `cms_partners` (
  `id` int(11) NOT NULL,
  `login` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `company` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_payments`
--

CREATE TABLE `cms_payments` (
  `id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `discounts_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `price` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_prices`
--

CREATE TABLE `cms_prices` (
  `id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `price_partner` double(5,2) NOT NULL,
  `price_driver` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_services`
--

CREATE TABLE `cms_services` (
  `id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_services`
--

INSERT INTO `cms_services` (`id`, `title`, `status`) VALUES
(1, 'Baki-Quba', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `cms_user`
--

CREATE TABLE `cms_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cms_user`
--

INSERT INTO `cms_user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'emin', '-kRaUEtw7zOAlS0ljh4rb4EFc04A1_l0', '$2y$13$ueyItqI8wqphIvwC29CWFu7vjZ9yyLuBM84u6cESzmXdETqvAj/2i', NULL, 'ahmedovemin065@gmail.com', 10, 1510310720, 1510310720);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(11) NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0- deaktiv\n1- aktive',
  `level` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1 - superadmin\n2 - user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1510310376),
('m130524_201442_init', 1510310386);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_cars`
--
ALTER TABLE `cms_cars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_car_cms_car_type1_idx` (`car_type_id`);

--
-- Indexes for table `cms_car_types`
--
ALTER TABLE `cms_car_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_discounts_cms_partners1_idx` (`partners_id`);

--
-- Indexes for table `cms_orders`
--
ALTER TABLE `cms_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_orders_cms_partners1_idx` (`partners_id`),
  ADD KEY `fk_cms_orders_cms_prices1_idx` (`prices_id`),
  ADD KEY `fk_cms_orders_cms_car1_idx` (`car_id`);

--
-- Indexes for table `cms_partners`
--
ALTER TABLE `cms_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_payments`
--
ALTER TABLE `cms_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_payments_cms_orders1_idx` (`orders_id`),
  ADD KEY `fk_cms_payments_cms_discounts1_idx` (`discounts_id`);

--
-- Indexes for table `cms_prices`
--
ALTER TABLE `cms_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cms_prices_cms_car_type_idx` (`car_type_id`),
  ADD KEY `fk_cms_prices_cms_direction1_idx` (`service_id`);

--
-- Indexes for table `cms_services`
--
ALTER TABLE `cms_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_user`
--
ALTER TABLE `cms_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_cars`
--
ALTER TABLE `cms_cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_car_types`
--
ALTER TABLE `cms_car_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_orders`
--
ALTER TABLE `cms_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_partners`
--
ALTER TABLE `cms_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_payments`
--
ALTER TABLE `cms_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_prices`
--
ALTER TABLE `cms_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_services`
--
ALTER TABLE `cms_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_user`
--
ALTER TABLE `cms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cms_cars`
--
ALTER TABLE `cms_cars`
  ADD CONSTRAINT `fk_cms_car_cms_car_type1` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_discounts`
--
ALTER TABLE `cms_discounts`
  ADD CONSTRAINT `fk_cms_discounts_cms_partners1` FOREIGN KEY (`partners_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_orders`
--
ALTER TABLE `cms_orders`
  ADD CONSTRAINT `fk_cms_orders_cms_car1` FOREIGN KEY (`car_id`) REFERENCES `cms_cars` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_partners1` FOREIGN KEY (`partners_id`) REFERENCES `cms_partners` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_orders_cms_prices1` FOREIGN KEY (`prices_id`) REFERENCES `cms_prices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_payments`
--
ALTER TABLE `cms_payments`
  ADD CONSTRAINT `fk_cms_payments_cms_discounts1` FOREIGN KEY (`discounts_id`) REFERENCES `cms_discounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_payments_cms_orders1` FOREIGN KEY (`orders_id`) REFERENCES `cms_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_prices`
--
ALTER TABLE `cms_prices`
  ADD CONSTRAINT `fk_cms_prices_cms_car_type` FOREIGN KEY (`car_type_id`) REFERENCES `cms_car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cms_prices_cms_direction1` FOREIGN KEY (`service_id`) REFERENCES `cms_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
